﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Mainspace.Repository.Database;
using System.Threading.Tasks;
using Mainspace.Models.Request;

namespace Mainspace.Controllers
{
    [ApiController]
    [Route("admin")]
    public class AdministrationController : ControllerBase
    {
        private readonly ILogger<AdministrationController> _logger;
        readonly AdministrationRepository _dbRepo;

        public AdministrationController(ILogger<AdministrationController> logger, AdministrationRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region Groups
        [Route("save-group"), HttpPost]
        public async Task<object> Any(SaveGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveGroup(request.Id, request.Name, request.Note);
        }

        [Route("get-group"), HttpPost]
        public async Task<object> Any(GetGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGroup(request.Id);
        }

        [Route("get-groups"), HttpPost]
        public async Task<object> Any(GetGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGroups();
        }

        [Route("delete-group"), HttpPost]
        public async Task<object> Any(DeleteGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteGroup(request.Id);
        }

        [Route("get-group-join-permissions"), HttpPost]
        public async Task<object> Any(GetGroupJoinPermissionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGroupJoinPermissions(request.GroupId);
        }

        [Route("get-group-join-roles"), HttpPost]
        public async Task<object> Any(GetGroupJoinRolesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGroupJoinRoles(request.GroupId);
        }

        [Route("save-group-join-role"), HttpPost]
        public async Task<object> Any(SaveGroupJoinRoleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveGroupJoinRole(request.GroupId, request.RoleId);
        }

        [Route("save-group-join-permission"), HttpPost]
        public async Task<object> Any(SaveGroupJoinPermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveGroupJoinPermission(request.GroupId, request.PermissionId);
        }

        [Route("delete-group-join-role"), HttpPost]
        public async Task<object> Any(DeleteGroupJoinRoleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteGroupJoinRole(request.Id);
        }

        [Route("delete-group-join-permission"), HttpPost]
        public async Task<object> Any(DeleteGroupJoinPermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteGroupJoinPermission(request.Id);
        }
        #endregion

        #region Users
        [Route("save-user"), HttpPost]
        public async Task<object> Any(SaveUserRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveUser(request.Id, request.GroupId, request.StatusId, request.EmployeeId, request.Username, request.Password, request.Note);
        }

        [Route("get-users"), HttpPost]
        public async Task<object> Any(GetUsersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetUsers();
        }

        [Route("get-user"), HttpPost]
        public async Task<object> Any(GetUserRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetUser(request.Id);
        }

        [Route("delete-user"), HttpPost]
        public async Task<object> Any(DeleteUserRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteUser(request.Id);
        }

        [Route("signin"), HttpPost]
        public async Task<object> Any(SignInRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SignIn(request.Username, request.Password);
        }

        [Route("get-user-join-permissions"), HttpPost]
        public async Task<object> Any(GetUserJoinPermissionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetUserJoinPermissions(request.UserId);
        }

        [Route("get-current-user-join-permissions"), HttpPost]
        public async Task<object> Any(GetCurrentUserJoinPermissionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCurrentUserJoinPermissions(request.UserId);
        }

        [Route("get-user-join-roles"), HttpPost]
        public async Task<object> Any(GetUserJoinRolesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetUserJoinRoles(request.UserId);
        }

        [Route("save-user-join-role"), HttpPost]
        public async Task<object> Any(SaveUserJoinRoleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveUserJoinRole(request.UserId, request.RoleId);
        }

        [Route("save-user-join-permission"), HttpPost]
        public async Task<object> Any(SaveUserJoinPermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveUserJoinPermission(request.UserId, request.PermissionId);
        }

        [Route("delete-user-join-role"), HttpPost]
        public async Task<object> Any(DeleteUserJoinRoleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteUserJoinRole(request.Id);
        }

        [Route("delete-user-join-permission"), HttpPost]
        public async Task<object> Any(DeleteUserJoinPermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteUserJoinPermission(request.Id);
        }

        [Route("get-user-join-employee"), HttpPost]
        public async Task<object> Any(GetUserJoinEmployeeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetUserJoinEmployee(request.UserId);
        }
        #endregion

        #region Roles
        [Route("save-role"), HttpPost]
        public async Task<object> Any(SaveRoleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveRole(request.Id, request.Name);
        }

        [Route("get-roles"), HttpPost]
        public async Task<object> Any(GetRolesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetRoles(request.Code, request.Name);
        }

        [Route("get-role"), HttpPost]
        public async Task<object> Any(GetRoleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetRole(request.Id);
        }

        [Route("delete-role"), HttpPost]
        public async Task<object> Any(DeleteRoleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteRole(request.Id);
        }

        [Route("get-role-join-permissions"), HttpPost]
        public async Task<object> Any(GetRoleJoinPermissionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetRoleJoinPermissions(request.Id);
        }

        [Route("save-role-join-permission"), HttpPost]
        public async Task<object> Any(SaveRoleJoinPermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveRoleJoinPermission(request.RoleId, request.PermissionId);
        }

        [Route("delete-role-join-permission"), HttpPost]
        public async Task<object> Any(DeleteRoleJoinPermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteRoleJoinPermission(request.Id);
        }
        #endregion

        #region Permissions
        [Route("save-permission"), HttpPost]
        public async Task<object> Any(SavePermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SavePermission(request.Id, request.Code, request.Name);
        }

        [Route("get-permissions"), HttpPost]
        public async Task<object> Any(GetPermissionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetPermissions(request.Code, request.Name);
        }

        [Route("delete-permission"), HttpPost]
        public async Task<object> Any(DeletePermissionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeletePermission(request.Id);
        }
        #endregion

        #region Notifications
        [Route("get-notifications-count"), HttpPost]
        public async Task<object> Any(GetNotificationsCountRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetNotificationsCount(request.UserId);
        }

        [Route("mark-as-read-notification"), HttpPost]
        public async Task<object> Any(MarkAsReadNotificationRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.MarkAsReadNotification(request.UserId, request.RefId, request.NotificationTypeId);
        }
        #endregion
    }
}
