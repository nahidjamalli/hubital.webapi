﻿using System.Threading.Tasks;
using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Mainspace.Controllers
{
    [Route("config")]
    [ApiController]
    public class ConfigurationController : ControllerBase
    {
        private readonly ILogger<ConfigurationController> _logger;
        readonly ConfigurationRepository _dbRepo;

        public ConfigurationController(ILogger<ConfigurationController> logger, ConfigurationRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region Companies
        [Route("get-companies"), HttpPost]
        public async Task<object> Any(GetCompaniesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCompanies();
        }

        [Route("save-company"), HttpPost]
        public async Task<object> Any(SaveCompanyRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveCompany(request.Id, request.Name, request.Note);
        }

        [Route("delete-company"), HttpPost]
        public async Task<object> Any(DeleteCompanyRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteCompany(request.Id);
        }
        #endregion

        #region Languages
        [Route("get-languages"), HttpPost]
        public async Task<object> Any(GetLanguagesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetLanguages();
        }

        [Route("save-language"), HttpPost]
        public async Task<object> Any(SaveLanguageRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveLanguage(request.Id, request.Name);
        }

        [Route("delete-language"), HttpPost]
        public async Task<object> Any(DeleteLanguageRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteLanguage(request.Id);
        }
        #endregion

        #region Statuses
        [Route("get-statuses"), HttpPost]
        public async Task<object> Any(GetStatusesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetStatuses(request.UserId, request.StatusTypeId);
        }
        #endregion

        #region Countries
        [Route("get-countries"), HttpPost]
        public async Task<object> Any(GetCountriesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCountries();
        }
        #endregion

        #region Cities
        [Route("get-cities"), HttpPost]
        public async Task<object> Any(GetCitiesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCities(request.CountryId);
        }
        #endregion

        #region ContactTypes
        [Route("get-contact-types"), HttpPost]
        public async Task<object> Any(GetContactTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetContactTypes();
        }
        #endregion

        #region AddressTypes
        [Route("get-address-types"), HttpPost]
        public async Task<object> Any(GetAddressTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAddressTypes();
        }
        #endregion

        #region BankAccountPostingGroups
        [Route("save-bank-account-posting-group"), HttpPost]
        public async Task<object> Any(SaveBankAccountPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveBankAccountPostingGroup(request.Id, request.Code, request.Name, request.BankAccountId);
        }

        [Route("delete-bank-account-posting-group"), HttpPost]
        public async Task<object> Any(DeleteBankAccountPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteBankAccountPostingGroup(request.Id);
        }

        [Route("get-bank-account-posting-groups"), HttpPost]
        public async Task<object> Any(GetBankAccountPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBankAccountPostingGroups();
        }
        #endregion

        #region CustomerPostingGroups
        [Route("save-customer-posting-group"), HttpPost]
        public async Task<object> Any(SaveCustomerPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveCustomerPostingGroup(request.Id, request.Code, request.Name, request.ReceivablesAccountId);
        }

        [Route("delete-customer-posting-group"), HttpPost]
        public async Task<object> Any(DeleteCustomerPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteCustomerPostingGroup(request.Id);
        }

        [Route("get-customer-posting-groups"), HttpPost]
        public async Task<object> Any(GetCustomerPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCustomerPostingGroups();
        }
        #endregion

        #region FixedAssetPostingGroups
        [Route("save-fixed-asset-posting-group"), HttpPost]
        public async Task<object> Any(SaveFixedAssetPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveFixedAssetPostingGroup(request.Id, request.Code, request.Name, request.AcquisitionCostAccountId);
        }

        [Route("delete-fixed-asset-posting-group"), HttpPost]
        public async Task<object> Any(DeleteFixedAssetPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteFixedAssetPostingGroup(request.Id);
        }

        [Route("get-fixed-asset-posting-groups"), HttpPost]
        public async Task<object> Any(GetFixedAssetPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetFixedAssetPostingGroups();
        }
        #endregion

        #region GeneralBusinessPostingGroups
        [Route("save-general-business-posting-group"), HttpPost]
        public async Task<object> Any(SaveGeneralBusinessPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveGeneralBusinessPostingGroup(request.Id, request.Code, request.Description);
        }

        [Route("delete-general-business-posting-group"), HttpPost]
        public async Task<object> Any(DeleteGeneralBusinessPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteGeneralBusinessPostingGroup(request.Id);
        }

        [Route("get-general-business-posting-groups"), HttpPost]
        public async Task<object> Any(GetGeneralBusinessPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGeneralBusinessPostingGroups();
        }
        #endregion

        #region GeneralProductPostingGroups
        [Route("save-general-product-posting-group"), HttpPost]
        public async Task<object> Any(SaveGeneralProductPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveGeneralProductPostingGroup(request.Id, request.Code, request.Description);
        }

        [Route("delete-general-product-posting-group"), HttpPost]
        public async Task<object> Any(DeleteGeneralProductPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteGeneralProductPostingGroup(request.Id);
        }

        [Route("get-general-product-posting-groups"), HttpPost]
        public async Task<object> Any(GetGeneralProductPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGeneralProductPostingGroups();
        }
        #endregion

        #region GeneralPostingSetups
        [Route("save-general-posting-setup"), HttpPost]
        public async Task<object> Any(SaveGeneralPostingSetupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveGeneralPostingSetup(request.Id, request.Code, request.GeneralBusinessPostingGroupId, request.GeneralProductPostingGroupId, request.SalesAccountId, request.PurchaseAccountId);
        }

        [Route("delete-general-posting-setup"), HttpPost]
        public async Task<object> Any(DeleteGeneralPostingSetupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteGeneralPostingSetup(request.Id);
        }

        [Route("get-general-posting-setups"), HttpPost]
        public async Task<object> Any(GetGeneralPostingSetupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGeneralPostingSetups();
        }
        #endregion

        #region InventoryPostingSetups
        [Route("save-inventory-posting-setup"), HttpPost]
        public async Task<object> Any(SaveInventoryPostingSetupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveInventoryPostingSetup(request.Id, request.Code, request.Name, request.InventoryAccountId, request.InventoryPostingGroupId, request.LocationId);
        }

        [Route("delete-inventory-posting-setup"), HttpPost]
        public async Task<object> Any(DeleteInventoryPostingSetupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteInventoryPostingSetup(request.Id);
        }

        [Route("get-inventory-posting-setups"), HttpPost]
        public async Task<object> Any(GetInventoryPostingSetupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetInventoryPostingSetups();
        }
        #endregion

        #region InventoryPostingGroups
        [Route("save-inventory-posting-group"), HttpPost]
        public async Task<object> Any(SaveInventoryPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveInventoryPostingGroup(request.Id, request.Code, request.Description);
        }

        [Route("delete-inventory-posting-group"), HttpPost]
        public async Task<object> Any(DeleteInventoryPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteInventoryPostingGroup(request.Id);
        }

        [Route("get-inventory-posting-groups"), HttpPost]
        public async Task<object> Any(GetInventoryPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetInventoryPostingGroups();
        }
        #endregion

        #region TaxBusinessPostingGroups
        [Route("save-tax-business-posting-group"), HttpPost]
        public async Task<object> Any(SaveTaxBusinessPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveTaxBusinessPostingGroup(request.Id, request.Code, request.Description);
        }

        [Route("delete-tax-business-posting-group"), HttpPost]
        public async Task<object> Any(DeleteTaxBusinessPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteTaxBusinessPostingGroup(request.Id);
        }

        [Route("get-tax-business-posting-groups"), HttpPost]
        public async Task<object> Any(GetTaxBusinessPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetTaxBusinessPostingGroups();
        }
        #endregion

        #region TaxPostingSetups
        [Route("save-tax-posting-setup"), HttpPost]
        public async Task<object> Any(SaveTaxPostingSetupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveTaxPostingSetup(request.Id, request.Code, request.Description, request.TaxBusinessPostingGroupId, request.TaxProductPostingGroupId, request.TaxPercent, request.SalesTaxAccountId, request.PurchaseTaxAccountId);
        }

        [Route("delete-tax-posting-setup"), HttpPost]
        public async Task<object> Any(DeleteTaxPostingSetupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteTaxPostingSetup(request.Id);
        }

        [Route("get-tax-posting-setups"), HttpPost]
        public async Task<object> Any(GetTaxPostingSetupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetTaxPostingSetups();
        }
        #endregion

        #region TaxProductPostingGroups
        [Route("save-tax-product-posting-group"), HttpPost]
        public async Task<object> Any(SaveTaxProductPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveTaxProductPostingGroup(request.Id, request.Code, request.Description);
        }

        [Route("delete-tax-product-posting-group"), HttpPost]
        public async Task<object> Any(DeleteTaxProductPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteTaxProductPostingGroup(request.Id);
        }

        [Route("get-tax-product-posting-groups"), HttpPost]
        public async Task<object> Any(GetTaxProductPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetTaxProductPostingGroups();
        }
        #endregion

        #region VendorPostingGroups
        [Route("save-vendor-posting-group"), HttpPost]
        public async Task<object> Any(SaveVendorPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveVendorPostingGroup(request.Id, request.Code, request.Description, request.PayablesAccountId);
        }

        [Route("delete-vendor-posting-group"), HttpPost]
        public async Task<object> Any(DeleteVendorPostingGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteVendorPostingGroup(request.Id);
        }

        [Route("get-vendor-posting-groups"), HttpPost]
        public async Task<object> Any(GetVendorPostingGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetVendorPostingGroups();
        }
        #endregion
    }
}
