﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("contract")]
    [ApiController]
    public class ContractManagementController : ControllerBase
    {
        private readonly ILogger<ContractManagementController> _logger;
        readonly ContractManagementRepository _dbRepo;

        public ContractManagementController(ILogger<ContractManagementController> logger, ContractManagementRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        [Route("save-contract-type"), HttpPost]
        public async Task<object> Any(SaveContractTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveContractType(request.Id, request.Name);
        }

        [Route("get-contract-types"), HttpPost]
        public async Task<object> Any(GetContractTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetContractTypes();
        }

        [Route("delete-contract-type"), HttpPost]
        public async Task<object> Any(DeleteContractTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteContractType(request.Id);
        }
    }
}
