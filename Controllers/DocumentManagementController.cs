﻿using Mainspace.Models.Request;
using Mainspace.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Mainspace.Controllers
{
    [Route("document")]
    [ApiController]
    public class DocumentManagementController : ControllerBase
    {
        private readonly Microsoft.Extensions.Logging.ILogger<DocumentManagementController> _logger;
        static string rootPath = @"D:\Stubborn Team\stp-hub-api\";
        //static string rootPath = @"\\172.16.1.63\umumi\HUB\";

        public DocumentManagementController(ILogger<DocumentManagementController> logger)
        {
            _logger = logger;
        }

        /*
         * --- REQUEST TYPES ---
         * GetDirContents
         * Remove
         * Download
         * UploadChunk
         */

        [Route("file-manager-employee-demand")]
        public async System.Threading.Tasks.Task<object> AnyAsync(string command, string arguments, int employeeDemandId)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", command, Request.Path);
#endif

            if (command == null)
            {
                command = Request.Form["command"].ToString();
                arguments = Request.Form["arguments"].ToString();
            }

            return command switch
            {
                "GetDirContents" => GetDirContents(employeeDemandId),
                "Remove" => Remove(arguments, employeeDemandId),
                "Download" => Download(arguments, employeeDemandId),
                "UploadChunk" => await Upload(arguments.Replace("\\", "").Replace("\"{", "{").Replace("}\"", "}"), employeeDemandId),
                _ => BadRequest(),
            };
        }

        public async System.Threading.Tasks.Task<object> Upload(string arguments, int employeeDemandId)
        {
            var fileArgs = Newtonsoft.Json.JsonConvert.DeserializeObject<FileArgumentsRequest>(arguments);
            string fullPath = rootPath + "EmployeeDemands\\" + employeeDemandId + "\\" + fileArgs.chunkMetadata.FileName;
            var file = Request.Form.Files[0];

            if (file.Length > 0)
            {
                System.IO.Directory.CreateDirectory(rootPath + "EmployeeDemands\\" + employeeDemandId);
                using (var stream = new System.IO.FileStream(fullPath, System.IO.FileMode.Append))
                {
                    await file.CopyToAsync(stream);
                }
            }

            return new FileArray
            {
                success = true,
                errorId = null
            };
        }

        object Download(string arguments, int employeeDemandId)
        {
            var fileArgs = Newtonsoft.Json.JsonConvert.DeserializeObject<FileArgumentsRequest>(arguments);

            var fileItems = new FileArray
            {
                success = true,
                errorId = null
            };

            foreach (var pil in fileArgs.pathInfoList)
            {
                foreach (var pi in pil)
                {
                    string fullPath = rootPath + "EmployeeDemands\\" + employeeDemandId + "\\" + pi.name;
                    FileStreamResult result = null;

                    if (System.IO.File.Exists(fullPath))
                    {
                        var stream = System.IO.File.OpenRead(fullPath);
                        result = new FileStreamResult(stream, new Microsoft.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream"));
                        result.FileDownloadName = pi.name;
                    }

                    return result;
                }
            }

            return fileItems;
        }

        object Remove(string arguments, int employeeDemandId)
        {
            var fileArgs = Newtonsoft.Json.JsonConvert.DeserializeObject<FileArgumentsRequest>(arguments);
            var fileItems = new FileArray
            {
                success = true,
                errorId = null
            };

            foreach (var pi in fileArgs.pathInfo)
            {
                string fullPath = rootPath + "EmployeeDemands\\" + employeeDemandId + "\\" + pi.name;

                if (System.IO.File.Exists(fullPath))
                    System.IO.File.Delete(fullPath);
            }

            return fileItems;
        }

        private object GetDirContents(int employeeDemandId)
        {
            var fileItems = new FileArray
            {
                success = true,
                errorId = null
            };

            string fullPath = rootPath + "EmployeeDemands\\" + employeeDemandId;

            if (System.IO.Directory.Exists(fullPath))
            {
                fileItems.result = new System.Collections.Generic.List<Result>();

                foreach (string file in System.IO.Directory.GetFiles(fullPath))
                {
                    System.IO.FileInfo fi = new System.IO.FileInfo(file);

                    fileItems.result.Add(new Result
                    {
                        url = file,
                        key = "EmployeeDemands",
                        name = fi.Name,
                        dateModified = fi.LastWriteTime,
                        isDirectory = false,
                        size = fi.Length,
                        hasSubDirectories = false
                    });
                }
            }

            return fileItems;
        }
    }
}
