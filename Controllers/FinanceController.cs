﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("finance")]
    [ApiController]
    public class FinanceController : ControllerBase
    {
        private readonly ILogger<FinanceController> _logger;
        readonly FinanceRepository _dbRepo;

        public FinanceController(ILogger<FinanceController> logger, FinanceRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region Banks
        [Route("get-banks"), HttpPost]
        public async Task<object> Any(GetBanksRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBanks();
        }

        [Route("save-bank"), HttpPost]
        public async Task<object> Any(SaveBankRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveBank(request.Id, request.Code, request.FullName, request.ShortName, request.SWIFTCode, request.Email, request.Website, request.Address, request.Contact, request.CorrespondentAccountCode, request.TIN, request.Note);
        }

        [Route("delete-bank"), HttpPost]
        public async Task<object> Any(DeleteBankRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteBank(request.Id);
        }
        #endregion

        #region BankAccounts
        [Route("get-bank-accounts"), HttpPost]
        public async Task<object> Any(GetBankAccountsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBankAccounts();
        }

        [Route("save-bank-account"), HttpPost]
        public async Task<object> Any(SaveBankAccountRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveBankAccount(request.Id, request.BankId, request.BankAccountNumber, request.CurrencyCode, request.Note);
        }

        [Route("delete-bank-account"), HttpPost]
        public async Task<object> Any(DeleteBankAccountRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteBankAccount(request.Id);
        }
        #endregion

        #region AnalysisSections
        [Route("save-analysis-section-join-dimension-group"), HttpPost]
        public async Task<object> Any(SaveAnalysisSectionJoinDimensionGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveAnalysisSectionJoinDimensionGroup(request.AnalysisSectionCode, request.DimensionGroupId);
        }

        [Route("get-analysis-sections"), HttpPost]
        public async Task<object> Any(GetAnalysisSectionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAnalysisSections();
        }

        [Route("get-analysis-section-join-dimension-groups"), HttpPost]
        public async Task<object> Any(GetAnalysisSectionJoinDimensionGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAnalysisSectionJoinDimensionGroups(request.AnalysisSectionCode);
        }

        [Route("delete-analysis-section-join-dimension-group"), HttpPost]
        public async Task<object> Any(DeleteAnalysisSectionJoinDimensionGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteAnalysisSectionJoinDimensionGroup(request.AnalysisSectionCode, request.DimensionGroupId);
        }
        #endregion

        #region DimensionGroups
        [Route("save-dimension-group"), HttpPost]
        public async Task<object> Any(SaveDimensionGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveDimensionGroup(request.Id, request.Code, request.Name);
        }

        [Route("get-dimension-groups"), HttpPost]
        public async Task<object> Any(GetDimensionGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetDimensionGroups();
        }

        [Route("delete-dimension-group"), HttpPost]
        public async Task<object> Any(DeleteDimensionGroupRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteDimensionGroup(request.Id);
        }
        #endregion

        #region AnalysisDimensions
        [Route("save-analysis-dimension"), HttpPost]
        public async Task<object> Any(SaveAnalysisDimensionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveAnalysisDimension(request.Id, request.ParentId, request.DimensionGroupId, request.Code, request.Name);
        }

        [Route("get-analysis-dimensions"), HttpPost]
        public async Task<object> Any(GetAnalysisDimensionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAnalysisDimensions(request.DimensionGroupId);
        }

        [Route("delete-analysis-dimension"), HttpPost]
        public async Task<object> Any(DeleteAnalysisDimensionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteAnalysisDimension(request.Id);
        }

        [Route("get-analysis-dimension"), HttpPost]
        public async Task<object> Any(GetAnalysisDimensionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAnalysisDimension(request.AnalysisDimensionId);
        }

        #endregion

        #region BalanceTypes
        [Route("get-balance-types"), HttpPost]
        public async Task<object> Any(GetBalanceTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBalanceTypes();
        }
        #endregion

        #region AccountCategories
        [Route("get-account-category"), HttpPost]
        public async Task<object> Any(GetAccountCategoryRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountCategory(request.Id);
        }

        [Route("get-account-categories"), HttpPost]
        public async Task<object> Any(GetAccountCategoriesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountCategories(request.Code, request.Name, request.PageNumber, request.RowCount);
        }

        [Route("delete-account-category"), HttpPost]
        public async Task<object> Any(DeleteAccountCategoryRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteAccountCategory(request.Id);
        }

        [Route("save-account-category"), HttpPost]
        public async Task<object> Any(SaveAccountCategoryRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveAccountCategory(request.Id, request.ParentId, request.Name);
        }

        [Route("get-account-category-parents"), HttpPost]
        public async Task<object> Any(GetAccountCategoryParentsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountCategoryParents();
        }
        #endregion

        #region Accounts
        [Route("get-account-parents"), HttpPost]
        public async Task<object> Any(GetAccountParentsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountParents();
        }

        [Route("get-accounts"), HttpPost]
        public async Task<object> Any(GetAccountsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccounts();
        }

        [Route("get-account-join-dimension-groups"), HttpPost]
        public async Task<object> Any(GetAccountJoinDimensionGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountJoinDimensionGroups(request.AccountId);
        }

        [Route("save-account-join-analysis-dimension"), HttpPost]
        public async Task<object> Any(SaveAccountJoinAnalysisDimensionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveAccountJoinAnalysisDimension(request.AccountId, request.DimensionGroupId, request.AnalysisDimensionId);
        }

        [Route("get-account"), HttpPost]
        public async Task<object> Any(GetAccountRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccount(request.AccountId);
        }

        [Route("save-account"), HttpPost]
        public async Task<object> Any(SaveAccountRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveAccount(request.Id, request.ParentId, request.Code, request.MapCode, request.Name, request.StatusCode, request.AccountGroupCode, request.AccountCategoryId, request.TransactionTypeCode, request.BalanceTypeCode, request.AccountTypeCode, request.DimensionGroups);
        }

        [Route("delete-account"), HttpPost]
        public async Task<object> Any(DeleteAccountRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteAccount(request.Id);
        }
        #endregion

        #region TransactionTypes
        [Route("get-transaction-types"), HttpPost]
        public async Task<object> Any(GetTransactionTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetTransactionTypes();
        }
        #endregion

        #region ElectronicInvoiceCodes
        [Route("get-electronic-invoice-codes"), HttpPost]
        public async Task<object> Any(GetElectronicInvoiceCodesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetElectronicInvoiceCodes(request.Code, request.Name, request.PageNumber, request.RowCount);
        }
        #endregion

        #region DimensionPostTypes
        [Route("get-dimension-post-types"), HttpPost]
        public async Task<object> Any(GetDimensionPostTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetDimensionPostTypes();
        }
        #endregion

        #region JournalTypes
        [Route("get-journal-types"), HttpPost]
        public async Task<object> Any(GetJournalTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJournalTypes();
        }

        [Route("delete-journal-type"), HttpPost]
        public async Task<object> Any(DeleteJournalTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteJournalType(request.Id);
        }

        [Route("save-journal-type"), HttpPost]
        public async Task<object> Any(SaveJournalTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveJournalType(request.Id, request.Code, request.Name);
        }
        #endregion

        #region Journals
        [Route("get-journals"), HttpPost]
        public async Task<object> Any(GetJournalsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJournals();
        }

        [Route("get-journal"), HttpPost]
        public async Task<object> Any(GetJournalRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJournal(request.Id);
        }

        [Route("delete-journal"), HttpPost]
        public async Task<object> Any(DeleteJournalRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteJournal(request.Id);
        }

        [Route("save-journal"), HttpPost]
        public async Task<object> Any(SaveJournalRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveJournal(request.Id, request.JournalTypeId, request.JournalModeId, request.Name, request.Note, request.StatusCode);
        }

        [Route("get-journal-lines"), HttpPost]
        public async Task<object> Any(GetJournalLinesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJournalLines(request.JournalId, request.JournalModeId);
        }

        [Route("save-journal-line"), HttpPost]
        public async Task<object> Any(SaveJournalLineRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveJournalLine(request.Id, request.JournalId, request.LineDate.ToLocalTime(), request.EntryExpirationDate?.ToLocalTime(), request.EntryTypeCode, request.DebitAccountId, request.CreditAccountId, request.Amount, request.PaidAmount, request.CurrencyCode, request.Note, request.ParentId, request.DocumentNumber);
        }

        [Route("save-journal-line-join-account-analysis-dimension"), HttpPost]
        public async Task<object> Any(SaveJournalLineJoinAccountAnalysisDimensionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveJournalLineJoinAccountAnalysisDimension(request.JournalLineId, request.AccountId, request.DimensionGroupId, request.AnalysisDimensionId);
        }

        [Route("get-journal-line-join-account-analysis-dimension"), HttpPost]
        public async Task<object> Any(GetJournalLineJoinAccountAnalysisDimensionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJournalLineJoinAccountAnalysisDimension(request.JournalLineId, request.AccountId, request.DimensionGroupId);
        }

        [Route("delete-journal-line"), HttpPost]
        public async Task<object> Any(DeleteJournalLineRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteJournalLine(request.JournalLineId);
        }

        [Route("get-journal-modes"), HttpPost]
        public async Task<object> Any(GetJournalModesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJournalModes();
        }
        #endregion

        #region Currencies
        [Route("get-currencies"), HttpPost]
        public async Task<object> Any(GetCurrenciesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCurrencies();
        }
        #endregion

        #region EntryTypes
        [Route("get-entry-types"), HttpPost]
        public async Task<object> Any(GetEntryTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEntryTypes();
        }
        #endregion

        #region AccountTypes
        [Route("get-account-types"), HttpPost]
        public async Task<object> Any(GetAccountTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountTypes();
        }
        #endregion

        #region AccountGroups
        [Route("get-account-groups"), HttpPost]
        public async Task<object> Any(GetAccountGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountGroups();
        }
        #endregion
    }
}
