﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("human")]
    [ApiController]
    public class HumanResourcesController : ControllerBase
    {
        readonly ILogger<HumanResourcesController> _logger;
        readonly HumanResourcesRepository _dbRepo;

        public HumanResourcesController(ILogger<HumanResourcesController> logger, HumanResourcesRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region Departments
        [Route("save-department"), HttpPost]
        public async Task<object> Any(SaveDepartmentRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveDepartment(request.Id, request.ParentId, request.StructureId, request.Name);
        }

        [Route("get-departments"), HttpPost]
        public async Task<object> Any(GetDepartmentsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetDepartments(request.StructureId);
        }

        [Route("delete-department"), HttpPost]
        public async Task<object> Any(DeleteDepartmentRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteDepartment(request.Id);
        }
        #endregion

        #region Positions
        [Route("save-position"), HttpPost]
        public async Task<object> Any(SavePositionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SavePosition(request.Id, request.ParentId, request.StructureId, request.Name);
        }

        [Route("get-positions"), HttpPost]
        public async Task<object> Any(GetPositionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetPositions(request.StructureId);
        }

        [Route("delete-position"), HttpPost]
        public async Task<object> Any(DeletePositionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeletePosition(request.Id);
        }
        #endregion

        #region Structures
        [Route("save-structure"), HttpPost]
        public async Task<object> Any(SaveStructureRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveStructure(request.Id, request.Name);
        }

        [Route("get-structures"), HttpPost]
        public async Task<object> Any(GetStructuresRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetStructures();
        }

        [Route("delete-structure"), HttpPost]
        public async Task<object> Any(DeleteStructureRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteStructure(request.Id);
        }
        #endregion

        #region Genders
        [Route("get-genders"), HttpPost]
        public async Task<object> Any(GetGendersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGenders();
        }
        #endregion

        #region EducationDegrees
        [Route("get-education-degrees"), HttpPost]
        public async Task<object> Any(GetEducationDegreesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEducationDegrees();
        }

        [Route("save-education-degree"), HttpPost]
        public async Task<object> Any(SaveEducationDegreeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveEducationDegree(request.Id, request.Name);
        }

        [Route("delete-education-degree"), HttpPost]
        public async Task<object> Any(DeleteEducationDegreeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteEducationDegree(request.Id);
        }
        #endregion

        #region SkillTypes
        [Route("get-skill-types"), HttpPost]
        public async Task<object> Any(GetSkillTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetSkillTypes();
        }

        [Route("save-skill-type"), HttpPost]
        public async Task<object> Any(SaveSkillTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveSkillType(request.Id, request.Name);
        }

        [Route("delete-skill-type"), HttpPost]
        public async Task<object> Any(DeleteSkillTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteSkillType(request.Id);
        }
        #endregion

        #region Skills
        [Route("get-skills"), HttpPost]
        public async Task<object> Any(GetSkillsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetSkills(request.SkillTypeCode);
        }

        [Route("save-skill"), HttpPost]
        public async Task<object> Any(SaveSkillRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveSkill(request.Id, request.SkillTypeId, request.Name);
        }

        [Route("delete-skill"), HttpPost]
        public async Task<object> Any(DeleteSkillRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteSkill(request.Id);
        }
        #endregion

        #region Reasons
        [Route("get-reasons"), HttpPost]
        public async Task<object> Any(GetReasonsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetReasons();
        }

        [Route("save-reason"), HttpPost]
        public async Task<object> Any(SaveReasonRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveReason(request.Id, request.Name);
        }

        [Route("delete-reason"), HttpPost]
        public async Task<object> Any(DeleteReasonRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteReason(request.Id);
        }
        #endregion

        #region EmployeeDemands
        [Route("is-readonly-employee-demand"), HttpPost]
        public async Task<object> Any(IsReadOnlyEmployeeDemandRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.IsReadOnlyEmployeeDemand(request.UserId, request.EmployeeDemandId);
        }

        [Route("get-employee-demands"), HttpPost]
        public async Task<object> Any(GetEmployeeDemandsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeDemands(request.UserId, request.Code, request.Name, request.Department, request.Position);
        }

        [Route("get-employee-demand"), HttpPost]
        public async Task<object> Any(GetEmployeeDemandRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeDemand(request.Id, request.UserId);
        }

        [Route("save-employee-demand"), HttpPost]
        public async Task<object> Any(SaveEmployeeDemandRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            var result = await _dbRepo.SaveEmployeeDemand(request.Id, request.Name, request.Note, request.ResponsibilityNote, request.GenderId, request.EmployeeCount, request.MinAge, request.MaxAge, request.MinExperience, request.MaxExperience, request.DepartmentId, request.PositionId, request.ContractTypeId, request.ReasonNote, request.EducationDegrees, request.Skills, request.ReasonId, request.ApprovalEmployees, request.Languages, request.Moderators, request.StatusId, request.StructureId, request.CreatorUserId);

            //_mailService.SendMailJoinEmployeeDemand(result.ROWID);

            return result;
        }

        [Route("delete-employee-demand"), HttpPost]
        public async Task<object> Any(DeleteEmployeeDemandRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteEmployeeDemand(request.Id);
        }

        [Route("get-employee-demand-join-skills"), HttpPost]
        public async Task<object> Any(GetEmployeeDemandJoinSkillsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeDemandJoinSkills(request.EmployeeDemandId);
        }

        [Route("get-employee-demand-join-education-degrees"), HttpPost]
        public async Task<object> Any(GetEmployeeDemandJoinEducationDegreesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeDemandJoinEducationDegrees(request.EmployeeDemandId);
        }

        [Route("get-employee-demand-join-languages"), HttpPost]
        public async Task<object> Any(GetEmployeeDemandJoinLanguagesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeDemandJoinLanguages(request.EmployeeDemandId);
        }

        [Route("get-employee-demand-join-approval-employees"), HttpPost]
        public async Task<object> Any(GetEmployeeDemandJoinApprovalEmployeesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeDemandJoinApprovalEmployees(request.EmployeeDemandId, request.IsModerator);
        }

        [Route("get-employee-demand-join-approval-employees-history"), HttpPost]
        public async Task<object> Any(GetEmployeeDemandJoinApprovalEmployeesHistoryRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeDemandJoinApprovalEmployeesHistory(request.EmployeeDemandId);
        }
        #endregion

        #region Employees
        [Route("create-as-employee"), HttpPost]
        public async Task<object> Any(CreateAsEmployeeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.CreateAsEmployee(request.JobOfferId);
        }

        [Route("get-employees"), HttpPost]
        public async Task<object> Any(GetEmployeesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployees(request.Code, request.Firstname, request.Middlename, request.Lastname, request.Position);
        }

        [Route("get-employee"), HttpPost]
        public async Task<object> Any(GetEmployeeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployee(request.Id);
        }

        [Route("save-employee"), HttpPost]
        public async Task<object> Any(SaveEmployeeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveEmployee(request.Id, request.JobOfferId, request.StatusId, request.StartDate, request.FirstName, request.MiddleName, request.LastName, request.IDNumber, request.SIN, request.TIN, request.PrimaryEmail, request.CitizenshipId, request.BirthCountryId, request.BirthCityId, request.BirthDate, request.GenderId, request.BloodGroupId, request.ReligionId, request.EmploymentTypeId, request.DrivingCategories, request.MilitaryStatusId, request.MaritalStatusId, request.Experience, request.Note);
        }

        [Route("delete-employee"), HttpPost]
        public async Task<object> Any(DeleteEmployeeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteEmployee(request.Id);
        }

        [Route("get-employee-join-driving-categories"), HttpPost]
        public async Task<object> Any(GetEmployeeJoinDrivingCategoriesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeeJoinDrivingCategories(request.EmployeeId);
        }

        [Route("get-employees-join-users"), HttpPost]
        public async Task<object> Any(GetEmployeesJoinUsersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmployeesJoinUsers(request.UserId, request.Username, request.Code, request.Firstname, request.Middlename, request.Lastname, request.Position);
        }
        #endregion

        #region Candidates
        [Route("get-candidates"), HttpPost]
        public async Task<object> Any(GetCandidatesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCandidates(request.Code, request.Firstname, request.Middlename, request.Lastname, request.Source);
        }

        [Route("get-candidate"), HttpPost]
        public async Task<object> Any(GetCandidateRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCandidate(request.Id);
        }

        [Route("save-candidate"), HttpPost]
        public async Task<object> Any(SaveCandidateRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveCandidate(request.Id, request.CreatorUserId, request.Firstname, request.Middlename, request.Lastname, request.EmploymentTypeId, request.Experience, request.SourceId, request.GenderId, request.Note, request.IDNumber, request.MaritalStatusId, request.MilitaryStatusId, request.CitizenshipId, request.BirthCountryId, request.BirthCityId, request.BirthDate, request.DrivingCategories, request.Languages, request.Contacts, request.Skills, request.EducationDegrees);
        }

        [Route("delete-candidate"), HttpPost]
        public async Task<object> Any(DeleteCandidateRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteCandidate(request.Id);
        }

        [Route("get-candidate-join-skills"), HttpPost]
        public async Task<object> Any(GetCandidateJoinSkillsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCandidateJoinSkills(request.CandidateId);
        }

        [Route("get-candidate-join-languages"), HttpPost]
        public async Task<object> Any(GetCandidateJoinLanguagesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCandidateJoinLanguages(request.CandidateId);
        }

        [Route("get-candidate-join-contacts"), HttpPost]
        public async Task<object> Any(GetCandidateJoinContactsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCandidateJoinContacts(request.CandidateId);
        }

        [Route("get-candidate-join-driving-categories"), HttpPost]
        public async Task<object> Any(GetCandidateJoinDrivingCategoriesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCandidateJoinDrivingCategories(request.CandidateId);
        }

        [Route("get-candidate-join-education-degrees"), HttpPost]
        public async Task<object> Any(GetCandidateJoinEducationDegreesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCandidateJoinEducationDegrees(request.CandidateId);
        }
        #endregion

        #region EmploymentTypes
        [Route("get-employment-types"), HttpPost]
        public async Task<object> Any(GetEmploymentTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetEmploymentTypes();
        }
        #endregion

        #region Sources
        [Route("get-sources"), HttpPost]
        public async Task<object> Any(GetSourcesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetSources();
        }
        #endregion

        #region MaritalStatuses
        [Route("get-marital-statuses"), HttpPost]
        public async Task<object> Any(GetMaritalStatusesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetMaritalStatuses();
        }
        #endregion

        #region MilitaryStatuses
        [Route("get-military-statuses"), HttpPost]
        public async Task<object> Any(GetMilitaryStatusesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetMilitaryStatuses();
        }
        #endregion

        #region DrivingCategories
        [Route("get-driving-categories"), HttpPost]
        public async Task<object> Any(GetDrivingCategoriesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetDrivingCategories();
        }
        #endregion

        #region CitizenshipList
        [Route("get-citizenship-list"), HttpPost]
        public async Task<object> Any(GetCitizenshipListRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCitizenshipList();
        }
        #endregion

        #region Ampplications
        [Route("get-applications"), HttpPost]
        public async Task<object> Any(GetApplicationsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetApplications(request.UserId, request.Code, request.EmployeeDemandCode, request.Department, request.Position);
        }

        [Route("get-application"), HttpPost]
        public async Task<object> Any(GetApplicationRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetApplication(request.Id);
        }

        [Route("save-application"), HttpPost]
        public async Task<object> Any(SaveApplicationRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveApplication(request.Id, request.CreatorUserId, request.EmployeeDemandId, request.CandidateId, request.ExpectedSalary, request.StatusId, request.Note, request.ApprovalEmployees, request.Moderators);
        }

        [Route("delete-application"), HttpPost]
        public async Task<object> Any(DeleteApplicationRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteApplication(request.Id);
        }

        [Route("get-application-join-interviews"), HttpPost]
        public async Task<object> Any(GetApplicationJoinInterviewsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetApplicationJoinInterviews(request.ApplicationId);
        }

        [Route("get-application-join-approval-employees"), HttpPost]
        public async Task<object> Any(GetApplicationJoinApprovalEmployeesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetApplicationJoinApprovalEmployees(request.ApplicationId, request.IsModerator);
        }

        [Route("is-readonly-application"), HttpPost]
        public async Task<object> Any(IsReadOnlyApplicationRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.IsReadOnlyApplication(request.UserId, request.ApplicationId);
        }

        #region Appointments
        [Route("get-application-join-appointments"), HttpPost]
        public async Task<object> Any(GetApplicationJoinAppointmentsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetApplicationJoinAppointments(request.UserId, request.ApplicationId);
        }

        [Route("save-appointment"), HttpPost]
        public async Task<object> Any(SaveAppointmentRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveAppointment(request.Id, request.CreatorUserId, request.ApplicationId, request.Name, request.Location, request.DecisionId, request.InterviewTypeId, request.FromDateTime, request.ToDateTime, request.Note);
        }

        [Route("delete-appointment"), HttpPost]
        public async Task<object> Any(DeleteAppointmentRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteAppointment(request.Id);
        }
        #endregion
        #endregion

        #region InterviewTypes
        [Route("get-interview-types"), HttpPost]
        public async Task<object> Any(GetInterviewTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetInterviewTypes();
        }
        #endregion

        #region JobOffers
        [Route("get-job-offers"), HttpPost]
        public async Task<object> Any(GetJobOffersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJobOffers(request.Code, request.ApplicationCode, request.Department, request.Position);
        }

        [Route("get-job-offer"), HttpPost]
        public async Task<object> Any(GetJobOfferRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetJobOffer(request.Id);
        }

        [Route("delete-job-offer"), HttpPost]
        public async Task<object> Any(DeleteJobOfferRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteJobOffer(request.Id);
        }

        [Route("save-job-offer"), HttpPost]
        public async Task<object> Any(SaveJobOfferRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveJobOffer(request.Id, request.ApplicationId, request.CreatorUserId, request.Amount, request.StatusId, request.OfferDate, request.OfferExpirationDate, request.Note);
        }
        #endregion

        #region ProbationPeriods
        [Route("get-probation-periods"), HttpPost]
        public async Task<object> Any(GetProbationPeriodsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetProbationPeriods(request.UserId);
        }

        [Route("get-probation-period"), HttpPost]
        public async Task<object> Any(GetProbationPeriodRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetProbationPeriod(request.Id);
        }

        [Route("delete-probation-period"), HttpPost]
        public async Task<object> Any(DeleteProbationPeriodRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteProbationPeriod(request.Id);
        }

        [Route("save-probation-period"), HttpPost]
        public async Task<object> Any(SaveProbationPeriodRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveProbationPeriod(request.Id, request.StatusId, request.EmployeeId, request.ApprovalEmployeeId, request.CreatorUserId, request.StartDate, request.EndDate, request.Note);
        }

        [Route("get-probation-period-join-skills"), HttpPost]
        public async Task<object> Any(GetProbationPeriodJoinSkillsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetProbationPeriodJoinSkills(request.SkillTypeCode, request.ProbationPeriodId);
        }

        [Route("save-probation-period-join-skills"), HttpPost]
        public async Task<object> Any(SaveProbationPeriodJoinSkillsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveProbationPeriodJoinSkills(request.SkillId, request.ProbationPeriodId, request.Evaluation);
        }

        [Route("is-readonly-probation-period"), HttpPost]
        public async Task<object> Any(IsReadOnlyProbationPeriodRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.IsReadOnlyProbationPeriod(request.UserId, request.ProbationPeriodId);
        }
        #endregion

        #region BloodGroups
        [Route("get-blood-groups"), HttpPost]
        public async Task<object> Any(GetBloodGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBloodGroups();
        }
        #endregion

        #region Religions
        [Route("get-religions"), HttpPost]
        public async Task<object> Any(GetReligionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetReligions();
        }
        #endregion
    }
}
