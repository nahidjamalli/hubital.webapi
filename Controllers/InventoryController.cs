﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("inventory")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly ILogger<InventoryController> _logger;
        readonly InventoryRepository _dbRepo;

        public InventoryController(ILogger<InventoryController> logger, InventoryRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region ItemTypes
        [Route("save-item-type"), HttpPost]
        public async Task<object> Any(SaveItemTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveItemType(request.Id, request.Name, request.SearchName, request.Note);
        }

        [Route("delete-item-type"), HttpPost]
        public async Task<object> Any(DeleteItemTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteItemType(request.Id);
        }

        [Route("get-item-types"), HttpPost]
        public async Task<object> Any(GetItemTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetItemTypes();
        }
        #endregion

        #region ItemCategories
        [Route("save-item-category"), HttpPost]
        public async Task<object> Any(SaveItemCategoryRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveItemCategory(request.Id, request.ParentId, request.Name, request.SearchName, request.Note);
        }

        [Route("delete-item-category"), HttpPost]
        public async Task<object> Any(DeleteItemCategoryRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteItemCategory(request.Id);
        }

        [Route("get-item-categories"), HttpPost]
        public async Task<object> Any(GetItemCategoriesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetItemCategories();
        }
        #endregion

        #region UnitOfMeasures
        [Route("save-unit-of-measure"), HttpPost]
        public async Task<object> Any(SaveUnitOfMeasureRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveUnitOfMeasure(request.Id, request.Name, request.Note);
        }

        [Route("delete-unit-of-measure"), HttpPost]
        public async Task<object> Any(DeleteUnitOfMeasureRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteUnitOfMeasure(request.Id);
        }

        [Route("get-unit-of-measures"), HttpPost]
        public async Task<object> Any(GetUnitOfMeasuresRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetUnitOfMeasures();
        }
        #endregion

        #region Items
        [Route("save-item"), HttpPost]
        public async Task<object> Any(SaveItemRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveItem(request.Id, request.Description, request.BaseUnitOfMeasureId, request.ItemCategoryId, request.ItemTypeId, request.UnitCost, request.UnitPrice);
        }

        [Route("delete-item"), HttpPost]
        public async Task<object> Any(DeleteItemRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteItem(request.Id);
        }

        [Route("get-item"), HttpPost]
        public async Task<object> Any(GetItemRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetItem(request.Id);
        }

        [Route("get-items"), HttpPost]
        public async Task<object> Any(GetItemsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetItems();
        }
        #endregion
    }
}
