﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("purchase")]
    [ApiController]
    public class PurchaseController : ControllerBase
    {
        private readonly ILogger<PurchaseController> _logger;
        readonly PurchaseRepository _dbRepo;

        public PurchaseController(ILogger<PurchaseController> logger, PurchaseRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region Vendors
        [Route("save-vendor"), HttpPost]
        public async Task<object> Any(SaveVendorRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveVendor(request.Id, request.Name, request.SearchName, request.Note, request.Contacts, request.Addresses);
        }

        [Route("delete-vendor"), HttpPost]
        public async Task<object> Any(DeleteVendorRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteVendor(request.Id);
        }

        [Route("get-vendors"), HttpPost]
        public async Task<object> Any(GetVendorsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetVendors();
        }

        [Route("get-vendor"), HttpPost]
        public async Task<object> Any(GetVendorRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetVendor(request.Id);
        }
        #endregion

        #region PurchaseOrders
        [Route("save-purchase-order"), HttpPost]
        public async Task<object> Any(SavePurchaseOrderRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SavePurchaseOrder(request.Id, request.VendorId);
        }

        [Route("delete-purchase-order"), HttpPost]
        public async Task<object> Any(DeletePurchaseOrderRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeletePurchaseOrder(request.Id);
        }

        [Route("get-purchase-orders"), HttpPost]
        public async Task<object> Any(GetPurchaseOrdersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetPurchaseOrders();
        }

        [Route("get-purchase-order"), HttpPost]
        public async Task<object> Any(GetPurchaseOrderRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetPurchaseOrder(request.Id);
        }
        #endregion

        #region PurchaseOrderLines
        [Route("save-purchase-order-line"), HttpPost]
        public async Task<object> Any(SavePurchaseOrderLineRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SavePurchaseOrderLine(request.Id, request.PurchaseOrderId, request.ItemId, request.Quantity, request.LocationId);
        }

        [Route("delete-purchase-order-line"), HttpPost]
        public async Task<object> Any(DeletePurchaseOrderLineRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeletePurchaseOrderLine(request.Id);
        }

        [Route("get-purchase-order-lines"), HttpPost]
        public async Task<object> Any(GetPurchaseOrderLinesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetPurchaseOrderLines();
        }
        #endregion

        #region BlanketPurchaseOrders
        [Route("save-blanket-purchase-order"), HttpPost]
        public async Task<object> Any(SaveBlanketPurchaseOrderRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveBlanketPurchaseOrder(request.Id, request.VendorId, request.DocumentDate, request.OrderDate, request.DueDate, request.VendorShipmentNumber
                , request.VendorOrderNumber);
        }

        [Route("delete-blanket-purchase-order"), HttpPost]
        public async Task<object> Any(DeleteBlanketPurchaseOrderRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteBlanketPurchaseOrder(request.Id);
        }

        [Route("get-blanket-purchase-orders"), HttpPost]
        public async Task<object> Any(GetBlanketPurchaseOrdersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBlanketPurchaseOrders();
        }

        [Route("get-blanket-purchase-order"), HttpPost]
        public async Task<object> Any(GetBlanketPurchaseOrderRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBlanketPurchaseOrder(request.Id);
        }
        #endregion

        #region BlanketPurchaseOrderLines
        [Route("save-blanket-purchase-order-line"), HttpPost]
        public async Task<object> Any(SaveBlanketPurchaseOrderLineRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveBlanketPurchaseOrderLine(request.Id, request.BlanketPurchaseOrderId, request.ItemId, request.UnitOfMeasureId, request.Quantity, request.LocationId, request.QuantityReceived, request.QuantityInvoiced, request.ExpectedReceiptDate);
        }

        [Route("delete-blanket-purchase-order-line"), HttpPost]
        public async Task<object> Any(DeleteBlanketPurchaseOrderLineRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteBlanketPurchaseOrderLine(request.Id);
        }

        [Route("get-blanket-purchase-order-lines"), HttpPost]
        public async Task<object> Any(GetBlanketPurchaseOrderLinesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBlanketPurchaseOrderLines(request.Id);
        }
        #endregion
    }
}
