﻿using System.Threading.Tasks;
using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Mainspace.Controllers
{
    [Route("report")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly ILogger<ReportController> _logger;
        readonly ReportRepository _dbRepo;

        public ReportController(ILogger<ReportController> logger, ReportRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region TrialBalance
        [Route("get-trial-balance"), HttpPost]
        public async Task<object> Any(GetTrialBalanceRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetTrialBalance(request.StartDate.ToLocalTime(), request.EndDate.ToLocalTime());
        }
        #endregion

        #region AccountListing
        [Route("get-account-listing"), HttpPost]
        public async Task<object> Any(GetAccountListingRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountListing(request.AccountId, request.StartDate.ToLocalTime(), request.EndDate.ToLocalTime());
        }
        #endregion

        #region GeneralLedgerHistory
        [Route("get-general-ledger-history"), HttpPost]
        public async Task<object> Any(GetGeneralLedgerHistoryRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetGeneralLedgerHistory(request.StartDate?.ToLocalTime(), request.EndDate?.ToLocalTime());
        }
        #endregion

        #region AccountsAging
        [Route("get-accounts-aging"), HttpPost]
        public async Task<object> Any(GetAccountsAgingRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAccountsAging(request.StartDate.ToLocalTime(), request.EndDate.ToLocalTime());
        }
        #endregion
    }
}
