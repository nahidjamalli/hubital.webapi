﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("sales")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly ILogger<SalesController> _logger;
        readonly SalesRepository _dbRepo;

        public SalesController(ILogger<SalesController> logger, SalesRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region Customers
        [Route("save-customer"), HttpPost]
        public async Task<object> Any(SaveCustomerRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveCustomer(request.Id, request.Name, request.SearchName, request.Note);
        }

        [Route("delete-customer"), HttpPost]
        public async Task<object> Any(DeleteCustomerRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteCustomer(request.Id);
        }

        [Route("get-customer"), HttpPost]
        public async Task<object> Any(GetCustomerRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCustomer(request.Id);
        }

        [Route("get-customers"), HttpPost]
        public async Task<object> Any(GetCustomersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCustomers();
        }
        #endregion
    }
}
