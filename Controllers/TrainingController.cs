﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("training")]
    [ApiController]
    public class TrainingController : ControllerBase
    {
        private readonly ILogger<TrainingController> _logger;
        readonly TrainingRepository _dbRepo;

        public TrainingController(ILogger<TrainingController> logger, TrainingRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region ExamTypes
        [Route("get-exam-types"), HttpPost]
        public async Task<object> Any(GetExamTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExamTypes();
        }
        #endregion

        #region ExamCandidates
        [Route("get-exam-candidates"), HttpPost]
        public async Task<object> Any(GetExamCandidatesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExamCandidates();
        }

        [Route("get-exam-candidate"), HttpPost]
        public async Task<object> Any(GetExamCandidateRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExamCandidate(request.Id);
        }

        [Route("save-exam-candidate"), HttpPost]
        public async Task<object> Any(SaveExamCandidateRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveExamCandidate(request.Id, request.ExamId, request.CandidateId, request.ShowResult, request.IsEmployee, request.Note);
        }

        [Route("delete-exam-candidate"), HttpPost]
        public async Task<object> Any(DeleteExamCandidateRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteExamCandidate(request.Id);
        }

        [Route("get-exam-candidate-join-answers"), HttpPost]
        public async Task<object> Any(GetExamCandidateJoinAnswersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExamCandidateJoinAnswers(request.ExamHistoryId, request.QuestionId);
        }
        #endregion

        #region Exams
        [Route("save-exam"), HttpPost]
        public async Task<object> Any(SaveExamRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveExam(request.Id, request.Name, request.ExamTypeId, request.LanguageId, request.RequiredScore, request.Duration, request.Note);
        }

        [Route("get-exams"), HttpPost]
        public async Task<object> Any(GetExamsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExams(request.Code, request.ExamType, request.Lanaguage, request.Name);
        }

        [Route("get-exam"), HttpPost]
        public async Task<object> Any(GetExamRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExam(request.Id);
        }

        [Route("get-current-exam"), HttpPost]
        public async Task<object> Any(GetCurrentExamRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCurrentExam(request.Id);
        }

        [Route("delete-exam"), HttpPost]
        public async Task<object> Any(DeleteExamRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteExam(request.Id);
        }

        [Route("delete-exam-join-rule"), HttpPost]
        public async Task<object> Any(DeleteExamJoinRuleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteExamJoinRule(request.RuleId, request.ExamId);
        }

        [Route("update-exam-join-question"), HttpPost]
        public async Task<object> Any(UpdateExamJoinQuestionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.UpdateExamJoinQuestion(request.Id, request.Order);
        }

        [Route("delete-exam-join-question"), HttpPost]
        public async Task<object> Any(DeleteExamJoinQuestionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteExamJoinQuestion(request.Id);
        }

        [Route("save-exam-join-rule"), HttpPost]
        public async Task<object> Any(SaveExamJoinRuleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveExamJoinRule(request.RuleId, request.ExamId);
        }

        [Route("save-exam-join-question"), HttpPost]
        public async Task<object> Any(SaveExamJoinQuestionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveExamJoinQuestion(request.QuestionId, request.ExamId);
        }

        [Route("get-exam-join-rules"), HttpPost]
        public async Task<object> Any(GetExamJoinRulesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExamJoinRules(request.Id);
        }

        [Route("get-exam-join-questions"), HttpPost]
        public async Task<object> Any(GetExamJoinQuestionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExamJoinQuestions(request.ExamId);
        }

        [Route("signin"), HttpPost]
        public async Task<object> Any(ExamCandidateSignInRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.ExamCandidateSignIn(request.Code);
        }

        [Route("start-exam"), HttpPost]
        public async Task<object> Any(StartExamRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.StartExam(request.Code);
        }

        [Route("stop-exam"), HttpPost]
        public async Task<object> Any(StopExamRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.StopExam(request.Id);
        }

        [Route("get-exam-result"), HttpPost]
        public async Task<object> Any(GetExamResultRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetExamResult(request.ExamHistoryId, request.ExamId);
        }
        #endregion

        #region Questions
        [Route("save-question"), HttpPost]
        public async Task<object> Any(SaveQuestionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveQuestion(request.Id, request.Name, request.QuestionGroupId, request.QuestionTypeId, request.Score, request.Duration, request.Note);
        }

        [Route("get-questions"), HttpPost]
        public async Task<object> Any(GetQuestionsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetQuestions(request.QuestionGroup, request.QuestionType, request.Code, request.Name);
        }

        [Route("get-question"), HttpPost]
        public async Task<object> Any(GetQuestionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetQuestion(request.Id);
        }

        [Route("delete-question"), HttpPost]
        public async Task<object> Any(DeleteQuestionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteQuestion(request.Id);
        }

        [Route("get-current-question"), HttpPost]
        public async Task<object> Any(GetCurrentQuestionRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCurrentQuestion(request.ExamId, request.Order);
        }

        [Route("get-question-types"), HttpPost]
        public async Task<object> Any(GetQuestionTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetQuestionTypes();
        }

        [Route("get-question-groups"), HttpPost]
        public async Task<object> Any(GetQuestionGroupsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetQuestionGroups();
        }

        [Route("get-question-join-contents"), HttpPost]
        public async Task<object> Any(GetQuestionJoinContentsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetQuestionJoinContents(request.QuestionId);
        }

        [Route("delete-content"), HttpPost]
        public async Task<object> Any(DeleteContentRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteContent(request.Id);
        }

        [Route("save-content"), HttpPost]
        public async Task<object> Any(SaveContentRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveContent(request.Id, request.QuestionId, request.AnswerId, request.Content, request.Type, request.Order);
        }
        #endregion

        #region Answers
        [Route("get-current-question-join-answers"), HttpPost]
        public async Task<object> Any(GetCurrentQuestionJoinAnswersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetCurrentQuestionJoinAnswers(request.Id);
        }

        [Route("delete-answer"), HttpPost]
        public async Task<object> Any(DeleteAnswerRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteAnswer(request.Id);
        }

        [Route("save-question-join-answers"), HttpPost]
        public async Task<object> Any(SaveQuestionJoinAnswersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            if (request.Answers != null && request.Answers.Length > 0) return await _dbRepo.SaveQuestionJoinAnswers(request.ExamHistoryId, request.TimeLeft, request.QuestionId, request.Answers);
            else if (!string.IsNullOrEmpty(request.Answer) && !string.IsNullOrWhiteSpace(request.Answer)) return await _dbRepo.SaveQuestionJoinOpenAnswer(request.ExamHistoryId, request.TimeLeft, request.QuestionId, request.Answer);
            else return null;
        }

        [Route("save-answer"), HttpPost]
        public async Task<object> Any(SaveAnswerRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveAnswer(request.Id, request.QuestionId, request.Name, request.Order, request.Correct);
        }

        [Route("get-question-join-answers"), HttpPost]
        public async Task<object> Any(GetQuestionJoinAnswersRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetQuestionJoinAnswers(request.QuestionId);
        }

        [Route("get-answer-join-contents"), HttpPost]
        public async Task<object> Any(GetAnswerJoinContentsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetAnswerJoinContents(request.AnswerId);
        }
        #endregion

        #region Rules
        [Route("get-rules"), HttpPost]
        public async Task<object> Any(GetRulesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetRules();
        }

        [Route("delete-rule"), HttpPost]
        public async Task<object> Any(DeleteRuleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteRule(request.Id);
        }

        [Route("save-rule"), HttpPost]
        public async Task<object> Any(SaveRuleRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveRule(request.Id, request.Code, request.Name);
        }
        #endregion
    }
}
