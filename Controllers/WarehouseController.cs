﻿using Mainspace.Models.Request;
using Mainspace.Repository.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Mainspace.Controllers
{
    [Route("warehouse")]
    [ApiController]
    public class WarehouseController : ControllerBase
    {
        private readonly ILogger<WarehouseController> _logger;
        readonly WarehouseRepository _dbRepo;

        public WarehouseController(ILogger<WarehouseController> logger, WarehouseRepository dbRepo)
        {
            _logger = logger;
            _dbRepo = dbRepo;
        }

        #region BinTypes
        [Route("save-bin-type"), HttpPost]
        public async Task<object> Any(SaveBinTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveBinType(request.Id, request.Code, request.Description, request.Receive, request.Ship, request.PutAway, request.Pick);
        }

        [Route("delete-bin-type"), HttpPost]
        public async Task<object> Any(DeleteBinTypeRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteBinType(request.Id);
        }

        [Route("get-bin-types"), HttpPost]
        public async Task<object> Any(GetBinTypesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBinTypes();
        }
        #endregion

        #region Zones
        [Route("save-zone"), HttpPost]
        public async Task<object> Any(SaveZoneRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveZone(request.Id, request.Code, request.Description, request.BinTypeId);
        }

        [Route("delete-zone"), HttpPost]
        public async Task<object> Any(DeleteZoneRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteZone(request.Id);
        }

        [Route("get-zones"), HttpPost]
        public async Task<object> Any(GetZonesRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetZones();
        }
        #endregion

        #region Bins
        [Route("save-bin"), HttpPost]
        public async Task<object> Any(SaveBinRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveBin(request.Id, request.Code, request.Description, request.ZoneId, request.BinTypeId);
        }

        [Route("delete-bin"), HttpPost]
        public async Task<object> Any(DeleteBinRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteBin(request.Id);
        }

        [Route("get-bins"), HttpPost]
        public async Task<object> Any(GetBinsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetBins();
        }
        #endregion

        #region Locations
        [Route("save-location"), HttpPost]
        public async Task<object> Any(SaveLocationRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.SaveLocation(request.Id, request.Name, request.SearchName);
        }

        [Route("delete-location"), HttpPost]
        public async Task<object> Any(DeleteLocationRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.DeleteLocation(request.Id);
        }

        [Route("get-locations"), HttpPost]
        public async Task<object> Any(GetLocationsRequest request)
        {
#if DEBUG
            _logger.LogInformation("{0} {1}", request.GetType().FullName, Request.Path);
#endif
            return await _dbRepo.GetLocations();
        }
        #endregion
    }
}
