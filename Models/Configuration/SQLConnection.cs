﻿namespace Mainspace.Models.Configuration
{
    public class SQLConnection
    {
        public string ConnectionString { get; set; }
    }
}
