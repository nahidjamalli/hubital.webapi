﻿namespace Mainspace.Models.Request
{
    public class DeleteAccountCategoryRequest
    {
        public int Id { get; set; }
    }
}
