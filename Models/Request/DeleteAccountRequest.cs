﻿namespace Mainspace.Models.Request
{
    public class DeleteAccountRequest
    {
        public int Id { get; set; }
    }
}
