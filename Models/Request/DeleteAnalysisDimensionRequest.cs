﻿namespace Mainspace.Models.Request
{
    public class DeleteAnalysisDimensionRequest
    {
        public int Id { get; set; }
    }
}
