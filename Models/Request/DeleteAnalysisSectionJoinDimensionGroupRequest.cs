﻿namespace Mainspace.Models.Request
{
    public class DeleteAnalysisSectionJoinDimensionGroupRequest
    {
        public int AnalysisSectionCode { get; set; }
        public int DimensionGroupId { get; set; }
    }
}
