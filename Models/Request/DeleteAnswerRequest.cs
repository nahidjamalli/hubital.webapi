﻿namespace Mainspace.Models.Request
{
    public class DeleteAnswerRequest
    {
        public int Id { get; set; }
    }
}
