﻿namespace Mainspace.Models.Request
{
    public class DeleteApplicationRequest
    {
        public int Id { get; set; }
    }
}
