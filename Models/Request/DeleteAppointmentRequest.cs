﻿namespace Mainspace.Models.Request
{
    public class DeleteAppointmentRequest
    {
        public int Id { get; set; }
    }
}
