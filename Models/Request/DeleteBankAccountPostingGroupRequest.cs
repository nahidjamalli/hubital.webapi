﻿namespace Mainspace.Models.Request
{
    public class DeleteBankAccountPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
