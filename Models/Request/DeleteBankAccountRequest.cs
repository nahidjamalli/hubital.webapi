﻿namespace Mainspace.Models.Request
{
    public class DeleteBankAccountRequest
    {
        public int Id { get; set; }
    }
}
