﻿namespace Mainspace.Models.Request
{
    public class DeleteBankRequest
    {
        public int Id { get; set; }
    }
}
