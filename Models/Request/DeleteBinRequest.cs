﻿namespace Mainspace.Models.Request
{
    public class DeleteBinRequest
    {
        public int Id { get; set; }
    }
}
