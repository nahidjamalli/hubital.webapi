﻿namespace Mainspace.Models.Request
{
    public class DeleteBlanketPurchaseOrderLineRequest
    {
        public int Id { get; set; }
    }
}
