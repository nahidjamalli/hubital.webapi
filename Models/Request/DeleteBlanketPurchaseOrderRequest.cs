﻿namespace Mainspace.Models.Request
{
    public class DeleteBlanketPurchaseOrderRequest
    {
        public int Id { get; set; }
    }
}
