﻿namespace Mainspace.Models.Request
{
    public class DeleteCandidateRequest
    {
        public int Id { get; set; }
    }
}
