﻿namespace Mainspace.Models.Request
{
    public class DeleteConditionRequest
    {
        public int Id { get; set; }
    }
}
