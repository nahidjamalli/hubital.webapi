﻿namespace Mainspace.Models.Request
{
    public class DeleteContentRequest
    {
        public int Id { get; set; }
    }
}
