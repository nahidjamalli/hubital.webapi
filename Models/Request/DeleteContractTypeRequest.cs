﻿namespace Mainspace.Models.Request
{
    public class DeleteContractTypeRequest
    {
        public int Id { get; set; }
    }
}
