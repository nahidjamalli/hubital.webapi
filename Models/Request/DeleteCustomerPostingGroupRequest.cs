﻿namespace Mainspace.Models.Request
{
    public class DeleteCustomerPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
