﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mainspace.Models.Request
{
    public class DeleteCustomerRequest
    {
        public int Id { get; set; }
    }
}
