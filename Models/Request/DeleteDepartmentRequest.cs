﻿namespace Mainspace.Models.Request
{
    public class DeleteDepartmentRequest
    {
        public int Id { get; set; }
    }
}
