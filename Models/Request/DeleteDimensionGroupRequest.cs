﻿namespace Mainspace.Models.Request
{
    public class DeleteDimensionGroupRequest
    {
        public int Id { get; set; }
    }
}
