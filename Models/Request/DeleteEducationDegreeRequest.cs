﻿namespace Mainspace.Models.Request
{
    public class DeleteEducationDegreeRequest
    {
        public int Id { get; set; }
    }
}
