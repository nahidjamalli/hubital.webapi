﻿namespace Mainspace.Models.Request
{
    public class DeleteEmployeeDemandRequest
    {
        public int Id { get; set; }
    }
}
