﻿namespace Mainspace.Models.Request
{
    public class DeleteEmployeeRequest
    {
        public int Id { get; set; }
    }
}
