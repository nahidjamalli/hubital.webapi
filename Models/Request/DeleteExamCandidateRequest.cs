﻿namespace Mainspace.Models.Request
{
    public class DeleteExamCandidateRequest
    {
        public int Id { get; set; }
    }
}
