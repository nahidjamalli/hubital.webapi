﻿namespace Mainspace.Models.Request
{
    public class DeleteExamJoinRuleRequest
    {
        public int RuleId { get; set; }
        public int ExamId { get; set; }
    }
}
