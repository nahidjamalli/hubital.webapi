﻿namespace Mainspace.Models.Request
{
    public class DeleteExamRequest
    {
        public int Id { get; set; }
    }
}
