﻿namespace Mainspace.Models.Request
{
    public class DeleteFixedAssetPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
