﻿namespace Mainspace.Models.Request
{
    public class DeleteGeneralBusinessPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
