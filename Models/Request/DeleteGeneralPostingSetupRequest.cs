﻿namespace Mainspace.Models.Request
{
    public class DeleteGeneralPostingSetupRequest
    {
        public int Id { get; set; }
    }
}
