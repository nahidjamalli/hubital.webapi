﻿namespace Mainspace.Models.Request
{
    public class DeleteGeneralProductPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
