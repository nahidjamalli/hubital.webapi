﻿namespace Mainspace.Models.Request
{
    public class DeleteGroupJoinPermissionRequest
    {
        public int Id { get; set; }
    }
}
