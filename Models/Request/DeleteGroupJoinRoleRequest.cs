﻿namespace Mainspace.Models.Request
{
    public class DeleteGroupJoinRoleRequest
    {
        public int Id { get; set; }
    }
}
