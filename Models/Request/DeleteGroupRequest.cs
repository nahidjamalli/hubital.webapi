﻿namespace Mainspace.Models.Request
{
    public class DeleteGroupRequest
    {
        public int Id { get; set; }
    }
}
