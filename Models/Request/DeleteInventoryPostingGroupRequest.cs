﻿namespace Mainspace.Models.Request
{
    public class DeleteInventoryPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
