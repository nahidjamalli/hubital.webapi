﻿namespace Mainspace.Models.Request
{
    public class DeleteItemCategoryRequest
    {
        public int Id { get; set; }
    }
}
