﻿namespace Mainspace.Models.Request
{
    public class DeleteItemRequest
    {
        public int Id { get; set; }
    }
}
