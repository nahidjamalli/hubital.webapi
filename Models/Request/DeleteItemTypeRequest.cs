﻿namespace Mainspace.Models.Request
{
    public class DeleteItemTypeRequest
    {
        public int Id { get; set; }
    }
}
