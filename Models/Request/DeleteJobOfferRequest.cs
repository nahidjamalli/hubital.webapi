﻿namespace Mainspace.Models.Request
{
    public class DeleteJobOfferRequest
    {
        public int Id { get; set; }
    }
}
