﻿namespace Mainspace.Models.Request
{
    public class DeleteJournalLineRequest
    {
        public int JournalLineId { get; set; }
    }
}
