﻿namespace Mainspace.Models.Request
{
    public class DeleteJournalRequest
    {
        public int Id { get; set; }
    }
}
