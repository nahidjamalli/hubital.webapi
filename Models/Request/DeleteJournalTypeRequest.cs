﻿namespace Mainspace.Models.Request
{
    public class DeleteJournalTypeRequest
    {
        public int Id { get; set; }
    }
}
