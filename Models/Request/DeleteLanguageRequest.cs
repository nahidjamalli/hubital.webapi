﻿namespace Mainspace.Models.Request
{
    public class DeleteLanguageRequest
    {
        public string Id { get; set; }
    }
}
