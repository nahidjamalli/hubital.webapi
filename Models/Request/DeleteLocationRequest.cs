﻿namespace Mainspace.Models.Request
{
    public class DeleteLocationRequest
    {
        public int Id { get; set; }
    }
}
