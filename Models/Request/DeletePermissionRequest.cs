﻿namespace Mainspace.Models.Request
{
    public class DeletePermissionRequest
    {
        public int Id { get; set; }
    }
}
