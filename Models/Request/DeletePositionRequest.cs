﻿namespace Mainspace.Models.Request
{
    public class DeletePositionRequest
    {
        public int Id { get; set; }
    }
}
