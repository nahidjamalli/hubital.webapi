﻿namespace Mainspace.Models.Request
{
    public class DeleteProbationPeriodRequest
    {
        public int Id { get; set; }
    }
}
