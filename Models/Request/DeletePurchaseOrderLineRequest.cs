﻿namespace Mainspace.Models.Request
{
    public class DeletePurchaseOrderLineRequest
    {
        public int Id { get; set; }
    }
}
