﻿namespace Mainspace.Models.Request
{
    public class DeletePurchaseOrderRequest
    {
        public int Id { get; set; }
    }
}
