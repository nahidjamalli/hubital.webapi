﻿namespace Mainspace.Models.Request
{
    public class DeleteQuestionRequest
    {
        public int Id { get; set; }
    }
}
