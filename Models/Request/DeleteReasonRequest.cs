﻿namespace Mainspace.Models.Request
{
    public class DeleteReasonRequest
    {
        public int Id { get; set; }
    }
}
