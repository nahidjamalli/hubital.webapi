﻿namespace Mainspace.Models.Request
{
    public class DeleteRoleRequest
    {
        public int Id { get; set; }
    }
}
