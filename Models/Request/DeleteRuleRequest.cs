﻿namespace Mainspace.Models.Request
{
    public class DeleteRuleRequest
    {
        public int Id { get; set; }
    }
}
