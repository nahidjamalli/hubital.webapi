﻿namespace Mainspace.Models.Request
{
    public class DeleteSkillRequest
    {
        public int Id { get; set; }
    }
}
