﻿namespace Mainspace.Models.Request
{
    public class DeleteSkillTypeRequest
    {
        public int Id { get; set; }
    }
}
