﻿namespace Mainspace.Models.Request
{
    public class DeleteStructureRequest
    {
        public int Id { get; set; }
    }
}
