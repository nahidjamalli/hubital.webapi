﻿namespace Mainspace.Models.Request
{
    public class DeleteTaxBusinessPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
