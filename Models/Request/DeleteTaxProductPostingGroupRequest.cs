﻿namespace Mainspace.Models.Request
{
    public class DeleteTaxProductPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
