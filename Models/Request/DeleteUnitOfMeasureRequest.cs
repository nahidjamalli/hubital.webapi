﻿namespace Mainspace.Models.Request
{
    public class DeleteUnitOfMeasureRequest
    {
        public int Id { get; set; }
    }
}
