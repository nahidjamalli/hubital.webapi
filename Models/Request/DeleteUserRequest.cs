﻿namespace Mainspace.Models.Request
{
    public class DeleteUserRequest
    {
        public int Id { get; set; }
    }
}
