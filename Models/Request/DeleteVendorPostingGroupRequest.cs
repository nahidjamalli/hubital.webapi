﻿namespace Mainspace.Models.Request
{
    public class DeleteVendorPostingGroupRequest
    {
        public int Id { get; set; }
    }
}
