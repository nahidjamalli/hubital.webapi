﻿namespace Mainspace.Models.Request
{
    public class DeleteVendorRequest
    {
        public int Id { get; set; }
    }
}
