﻿namespace Mainspace.Models.Request
{
    public class DeleteWarehouseRequest
    {
        public int Id { get; set; }
    }
}
