﻿namespace Mainspace.Models.Request
{
    public class DeleteZoneRequest
    {
        public int Id { get; set; }
    }
}
