﻿namespace Mainspace.Models.Request
{
    public class ExamCandidateSignInRequest
    {
        public string Code { get; set; }
    }
}
