﻿namespace Mainspace.Models.Request
{
    public class FileArgumentsRequest
    {
        public string command { get; set; }
        public PathInfo[] pathInfo { get; set; }
        public PathInfo[][] pathInfoList { get; set; }
        public string destinationId { get; set; }
        public ChunkMetadata chunkMetadata { get; set; }
    }

    public class ChunkMetadata
    {
        public string UploadId { get; set; }
        public string FileName { get; set; }
        public int Index { get; set; }
        public int TotalCount { get; set; }
        public int FileSize { get; set; }
    }

    public class PathInfo
    {
        public string key { get; set; }
        public string name { get; set; }
    }
}
