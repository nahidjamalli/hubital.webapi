﻿namespace Mainspace.Models.Request
{
    public class GetAccountCategoriesRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int PageNumber { get; set; }
        public int RowCount { get; set; }
    }
}
