﻿using System;

namespace Mainspace.Models.Request
{
    public class GetAccountListingRequest
    {
        public int AccountId { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}
