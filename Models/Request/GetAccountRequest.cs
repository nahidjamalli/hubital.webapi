﻿namespace Mainspace.Models.Request
{
    public class GetAccountRequest
    {
        public int AccountId { get; set; }
    }
}
