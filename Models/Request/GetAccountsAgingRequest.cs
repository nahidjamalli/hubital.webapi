﻿using System;

namespace Mainspace.Models.Request
{
    public class GetAccountsAgingRequest
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}
