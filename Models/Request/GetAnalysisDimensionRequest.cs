﻿namespace Mainspace.Models.Request
{
    public class GetAnalysisDimensionRequest
    {
        public int AnalysisDimensionId { get; set; }
    }
}
