﻿namespace Mainspace.Models.Request
{
    public class GetAnalysisDimensionsRequest
    {
        public int DimensionGroupId { get; set; }
    }
}
