﻿namespace Mainspace.Models.Request
{
    public class GetAnalysisSectionJoinDimensionGroupsRequest
    {
        public int AnalysisSectionCode { get; set; }
    }
}
