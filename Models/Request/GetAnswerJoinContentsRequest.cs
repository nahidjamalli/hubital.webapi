﻿namespace Mainspace.Models.Request
{
    public class GetAnswerJoinContentsRequest
    {
        public int AnswerId { get; set; }
    }
}
