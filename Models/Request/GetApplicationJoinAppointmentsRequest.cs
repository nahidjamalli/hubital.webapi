﻿namespace Mainspace.Models.Request
{
    public class GetApplicationJoinAppointmentsRequest
    {
        public int UserId { get; set; }
        public int ApplicationId { get; set; }
    }
}
