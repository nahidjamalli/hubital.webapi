﻿namespace Mainspace.Models.Request
{
    public class GetApplicationJoinApprovalEmployeesRequest
    {
        public int ApplicationId { get; set; }
        public bool IsModerator { get; set; }
    }
}
