﻿namespace Mainspace.Models.Request
{
    public class GetApplicationJoinInterviewsRequest
    {
        public int ApplicationId { get; set; }
    }
}
