﻿namespace Mainspace.Models.Request
{
    public class GetApplicationsRequest
    {
        public int? UserId { get; set; }
        public string Code { get; set; }
        public string EmployeeDemandCode { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
    }
}
