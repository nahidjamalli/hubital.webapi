﻿namespace Mainspace.Models.Request
{
    public class GetCandidateJoinContactsRequest
    {
        public int CandidateId { get; set; }
    }
}
