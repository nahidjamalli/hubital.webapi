﻿namespace Mainspace.Models.Request
{
    public class GetCandidateJoinDrivingCategoriesRequest
    {
        public int CandidateId { get; set; }
    }
}
