﻿namespace Mainspace.Models.Request
{
    public class GetCandidateJoinEducationDegreesRequest
    {
        public int CandidateId { get; set; }
    }
}
