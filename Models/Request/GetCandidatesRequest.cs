﻿namespace Mainspace.Models.Request
{
    public class GetCandidatesRequest
    {
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Source { get; set; }
    }
}
