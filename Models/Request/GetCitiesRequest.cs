﻿namespace Mainspace.Models.Request
{
    public class GetCitiesRequest
    {
        public int CountryId { get; set; }
    }
}
