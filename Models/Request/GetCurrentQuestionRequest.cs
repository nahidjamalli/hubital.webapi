﻿namespace Mainspace.Models.Request
{
    public class GetCurrentQuestionRequest
    {
        public int ExamId { get; set; }
        public int Order { get; set; }
    }
}
