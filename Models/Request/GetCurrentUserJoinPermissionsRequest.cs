﻿namespace Mainspace.Models.Request
{
    public class GetCurrentUserJoinPermissionsRequest
    {
        public int UserId { get; set; }
    }
}
