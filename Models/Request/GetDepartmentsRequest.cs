﻿namespace Mainspace.Models.Request
{
    public class GetDepartmentsRequest
    {
        public int? StructureId { get; set; }
    }
}
