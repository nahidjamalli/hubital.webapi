﻿namespace Mainspace.Models.Request
{
    public class GetElectronicInvoiceCodesRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int PageNumber { get; set; }
        public int RowCount { get; set; }
    }
}
