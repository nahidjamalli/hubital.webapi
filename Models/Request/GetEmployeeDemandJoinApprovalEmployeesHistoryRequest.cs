﻿namespace Mainspace.Models.Request
{
    public class GetEmployeeDemandJoinApprovalEmployeesHistoryRequest
    {
        public int EmployeeDemandId { get; set; }
    }
}
