﻿namespace Mainspace.Models.Request
{
    public class GetEmployeeDemandJoinApprovalEmployeesRequest
    {
        public int EmployeeDemandId { get; set; }
        public bool IsModerator { get; set; }
    }
}
