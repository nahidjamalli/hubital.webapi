﻿namespace Mainspace.Models.Request
{
    public class GetEmployeeDemandJoinConditionsRequest
    {
        public int EmployeeDemandId { get; set; }
    }
}
