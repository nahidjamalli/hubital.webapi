﻿namespace Mainspace.Models.Request
{
    public class GetEmployeeDemandRequest
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
    }
}
