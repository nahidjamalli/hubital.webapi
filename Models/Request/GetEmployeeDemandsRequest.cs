﻿namespace Mainspace.Models.Request
{
    public class GetEmployeeDemandsRequest
    {
        public int UserId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
    }
}
