﻿namespace Mainspace.Models.Request
{
    public class GetEmployeesJoinUsersRequest
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Position { get; set; }
    }
}
