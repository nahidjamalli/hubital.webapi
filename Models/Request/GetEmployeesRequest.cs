﻿namespace Mainspace.Models.Request
{
    public class GetEmployeesRequest
    {
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Position { get; set; }
    }
}
