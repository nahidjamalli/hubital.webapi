﻿namespace Mainspace.Models.Request
{
    public class GetExamCandidateJoinAnswersRequest
    {
        public int ExamHistoryId { get; set; }
        public int QuestionId { get; set; }
    }
}
