﻿namespace Mainspace.Models.Request
{
    public class GetExamResultRequest
    {
        public int ExamId { get; set; }
        public int ExamHistoryId { get; set; }
    }
}
