﻿namespace Mainspace.Models.Request
{
    public class GetExamsRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ExamType { get; set; }
        public string Lanaguage { get; set; }
    }
}
