﻿using System;

namespace Mainspace.Models.Request
{
    public class GetGeneralLedgerHistoryRequest
    {
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
    }
}
