﻿namespace Mainspace.Models.Request
{
    public class GetGroupJoinPermissionsRequest
    {
        public int GroupId { get; set; }
    }
}
