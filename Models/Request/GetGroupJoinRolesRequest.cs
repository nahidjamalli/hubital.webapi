﻿namespace Mainspace.Models.Request
{
    public class GetGroupJoinRolesRequest
    {
        public int GroupId { get; set; }
    }
}
