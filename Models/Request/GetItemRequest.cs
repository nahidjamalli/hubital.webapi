﻿namespace Mainspace.Models.Request
{
    public class GetItemRequest
    {
        public int Id { get; set; }
    }
}
