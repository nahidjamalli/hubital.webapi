﻿namespace Mainspace.Models.Request
{
    public class GetJobOffersRequest
    {
        public string Code { get; set; }
        public string ApplicationCode { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
    }
}
