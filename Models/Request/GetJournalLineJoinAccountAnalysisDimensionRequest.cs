﻿namespace Mainspace.Models.Request
{
    public class GetJournalLineJoinAccountAnalysisDimensionRequest
    {
        public int JournalLineId { get; set; }
        public int AccountId { get; set; }
        public int DimensionGroupId { get; set; }
    }
}
