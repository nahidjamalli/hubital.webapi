﻿namespace Mainspace.Models.Request
{
    public class GetJournalLinesRequest
    {
        public int JournalId { get; set; }
        public int JournalModeId { get; set; }
    }
}
