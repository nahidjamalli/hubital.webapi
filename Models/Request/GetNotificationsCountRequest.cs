﻿namespace Mainspace.Models.Request
{
    public class GetNotificationsCountRequest
    {
        public int UserId { get; set; }
    }
}
