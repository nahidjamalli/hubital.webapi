﻿namespace Mainspace.Models.Request
{
    public class GetPermissionsRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
