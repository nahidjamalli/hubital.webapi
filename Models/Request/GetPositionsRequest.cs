﻿namespace Mainspace.Models.Request
{
    public class GetPositionsRequest
    {
        public int? StructureId { get; set; }
    }
}
