﻿namespace Mainspace.Models.Request
{
    public class GetProbationPeriodJoinSkillsRequest
    {
        public int? ProbationPeriodId { get; set; }
        public int SkillTypeCode { get; set; }
    }
}
