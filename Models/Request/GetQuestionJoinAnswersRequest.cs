﻿namespace Mainspace.Models.Request
{
    public class GetQuestionJoinAnswersRequest
    {
        public int QuestionId { get; set; }
    }
}
