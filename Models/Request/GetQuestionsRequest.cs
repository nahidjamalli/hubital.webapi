﻿namespace Mainspace.Models.Request
{
    public class GetQuestionsRequest
    {
        public string QuestionType { get; set; }
        public string QuestionGroup { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
