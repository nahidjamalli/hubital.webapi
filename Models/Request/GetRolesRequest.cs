﻿namespace Mainspace.Models.Request
{
    public class GetRolesRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
