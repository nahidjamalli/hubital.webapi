﻿namespace Mainspace.Models.Request
{
    public class GetSkillsRequest
    {
        public int SkillTypeCode { get; set; }
    }
}
