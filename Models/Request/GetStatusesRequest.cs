﻿namespace Mainspace.Models.Request
{
    public class GetStatusesRequest
    {
        public int UserId { get; set; }
        public int StatusTypeId { get; set; }
    }
}
