﻿namespace Mainspace.Models.Request
{
    public class GetTrialBalanceRequest
    {
        public System.DateTimeOffset StartDate { get; set; }
        public System.DateTimeOffset EndDate { get; set; }
    }
}
