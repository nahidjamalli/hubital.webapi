﻿namespace Mainspace.Models.Request
{
    public class GetUserJoinPermissionsRequest
    {
        public int UserId { get; set; }
    }
}
