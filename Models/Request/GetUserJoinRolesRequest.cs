﻿namespace Mainspace.Models.Request
{
    public class GetUserJoinRolesRequest
    {
        public int UserId { get; set; }
    }
}
