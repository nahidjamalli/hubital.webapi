﻿namespace Mainspace.Models.Request
{
    public class IsReadOnlyApplicationRequest
    {
        public int UserId { get; set; }
        public int ApplicationId { get; set; }
    }
}
