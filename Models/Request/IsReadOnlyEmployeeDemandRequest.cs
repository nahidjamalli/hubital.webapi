﻿namespace Mainspace.Models.Request
{
    public class IsReadOnlyEmployeeDemandRequest
    {
        public int EmployeeDemandId { get; set; }
        public int UserId { get; set; }
    }
}
