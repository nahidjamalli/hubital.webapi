﻿namespace Mainspace.Models.Request
{
    public class IsReadOnlyProbationPeriodRequest
    {
        public int ProbationPeriodId { get; set; }
        public int UserId { get; set; }
    }
}
