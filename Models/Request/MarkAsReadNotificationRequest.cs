﻿namespace Mainspace.Models.Request
{
    public class MarkAsReadNotificationRequest
    {
        public int UserId { get; set; }
        public int RefId { get; set; }
        public int NotificationTypeId { get; set; }
    }
}
