﻿namespace Mainspace.Models.Request
{
    public class SaveAccountCategoryRequest
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
    }
}
