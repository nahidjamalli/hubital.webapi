﻿namespace Mainspace.Models.Request
{
    public class SaveAccountJoinAnalysisDimensionRequest
    {
        public int AccountId { get; set; }
        public int DimensionGroupId { get; set; }
        public int AnalysisDimensionId { get; set; }
    }
}
