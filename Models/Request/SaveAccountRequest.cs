﻿using Mainspace.Models.Response;
using System.Collections.Generic;

namespace Mainspace.Models.Request
{
    public class SaveAccountRequest
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int AccountCategoryId { get; set; }
        public string Code { get; set; }
        public int AccountGroupCode { get; set; }
        public string MapCode { get; set; }
        public string Name { get; set; }
        public int StatusCode { get; set; }
        public int BalanceTypeCode { get; set; }
        public int AccountTypeCode { get; set; }
        public int TransactionTypeCode { get; set; }
        public List<DimensionGroupPostTypeArray> DimensionGroups { get; set; }
    }
}
