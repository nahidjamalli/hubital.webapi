﻿namespace Mainspace.Models.Request
{
    public class SaveAnalysisDimensionRequest
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int DimensionGroupId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
