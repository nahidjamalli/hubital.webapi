﻿namespace Mainspace.Models.Request
{
    public class SaveAnalysisSectionJoinDimensionGroupRequest
    {
        public int AnalysisSectionCode { get; set; }
        public int DimensionGroupId { get; set; }
    }
}
