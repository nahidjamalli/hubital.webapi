﻿namespace Mainspace.Models.Request
{
    public class SaveAnswerRequest
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Name { get; set; }
        public byte Order { get; set; }
        public bool Correct { get; set; }
    }
}
