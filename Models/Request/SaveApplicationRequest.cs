﻿namespace Mainspace.Models.Request
{
    public class SaveApplicationRequest
    {
        public int Id { get; set; }
        public int EmployeeDemandId { get; set; }
        public int CreatorUserId { get; set; }
        public int CandidateId { get; set; }
        public int ExpectedSalary { get; set; }
        public int StatusId { get; set; }
        public string Note { get; set; }
        public int[] ApprovalEmployees { get; set; }
        public int[] Moderators { get; set; }
    }
}
