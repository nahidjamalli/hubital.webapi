﻿namespace Mainspace.Models.Request
{
    public class SaveAppointmentRequest
    {
        public int Id { get; set; }
        public int CreatorUserId { get; set; }
        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public int DecisionId { get; set; }
        public int InterviewTypeId { get; set; }
        public string FromDateTime { get; set; }
        public string ToDateTime { get; set; }
        public string Note { get; set; }
    }
}
