﻿namespace Mainspace.Models.Request
{
    public class SaveBankAccountPostingGroupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int BankAccountId { get; set; }
    }
}
