﻿namespace Mainspace.Models.Request
{
    public class SaveBankAccountRequest
    {
        public int Id { get; set; }
        public int BankId { get; set; }
        public string BankAccountNumber { get; set; }
        public string CurrencyCode { get; set; }
        public string Note { get; set; }
    }
}
