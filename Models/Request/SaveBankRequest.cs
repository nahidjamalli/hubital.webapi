﻿namespace Mainspace.Models.Request
{
    public class SaveBankRequest
    {
        public int Id { get; set; }
        public string Code{ get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string SWIFTCode { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string CorrespondentAccountCode { get; set; }
        public string TIN { get; set; }
        public string Note { get; set; }
    }
}
