﻿namespace Mainspace.Models.Request
{
    public class SaveBinRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int ZoneId { get; set; }
        public int BinTypeId { get; set; }
    }
}
