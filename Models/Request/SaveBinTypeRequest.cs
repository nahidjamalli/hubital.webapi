﻿namespace Mainspace.Models.Request
{
    public class SaveBinTypeRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Receive { get; set; }
        public bool Ship { get; set; }
        public bool PutAway { get; set; }
        public bool Pick { get; set; }
    }
}
