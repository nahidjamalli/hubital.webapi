﻿namespace Mainspace.Models.Request
{
    public class SaveBlanketPurchaseOrderLineRequest
    {
        public int Id { get; set; }
        public int BlanketPurchaseOrderId { get; set; }
        public int ItemId { get; set; }
        public int UnitOfMeasureId { get; set; }
        public decimal Quantity { get; set; }
        public int LocationId { get; set; }
        public decimal QuantityReceived { get; set; }
        public decimal QuantityInvoiced { get; set; }
        public System.DateTime ExpectedReceiptDate { get; set; }
    }
}
