﻿namespace Mainspace.Models.Request
{
    public class SaveBlanketPurchaseOrderRequest
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public System.DateTime OrderDate { get; set; }
        public System.DateTime DueDate { get; set; }
        public string VendorShipmentNumber { get; set; }
        public string VendorOrderNumber { get; set; }
    }
}
