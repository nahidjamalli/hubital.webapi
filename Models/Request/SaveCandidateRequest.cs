﻿using Mainspace.Models.Response;
using System.Collections.Generic;

namespace Mainspace.Models.Request
{
    public class SaveCandidateRequest
    {
        public int Id { get; set; }
        public int CreatorUserId { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public int? EmploymentTypeId { get; set; }
        public int? Experience { get; set; }
        public int? SourceId { get; set; }
        public int? GenderId { get; set; }
        public string Note { get; set; }
        public string IDNumber { get; set; }
        public int? MaritalStatusId { get; set; }
        public int? MilitaryStatusId { get; set; }
        public int? CitizenshipId { get; set; }
        public int? BirthCountryId { get; set; }
        public int? BirthCityId { get; set; }
        public string BirthDate { get; set; }
        public int[] DrivingCategories { get; set; }
        public int[] Skills { get; set; }
        public int[] EducationDegrees { get; set; }
        public IEnumerable<LanguageArray> Languages { get; set; }
        public IEnumerable<ContactArray> Contacts { get; set; }
    }
}
