﻿namespace Mainspace.Models.Request
{
    public class SaveConditionRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
