﻿namespace Mainspace.Models.Request
{
    public class SaveContentRequest
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public string Content { get; set; }
        public short Type { get; set; }
        public byte Order { get; set; }
    }
}
