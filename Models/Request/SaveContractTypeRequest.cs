﻿namespace Mainspace.Models.Request
{
    public class SaveContractTypeRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
