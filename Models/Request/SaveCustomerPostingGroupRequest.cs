﻿namespace Mainspace.Models.Request
{
    public class SaveCustomerPostingGroupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int ReceivablesAccountId { get; set; }
    }
}
