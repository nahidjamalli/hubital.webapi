﻿namespace Mainspace.Models.Request
{
    public class SaveCustomerRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SearchName { get; set; }
        public string Note { get; set; }
    }
}
