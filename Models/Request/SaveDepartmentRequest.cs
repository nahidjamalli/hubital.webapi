﻿namespace Mainspace.Models.Request
{
    public class SaveDepartmentRequest
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int StructureId { get; set; }
        public string Name { get; set; }
    }
}
