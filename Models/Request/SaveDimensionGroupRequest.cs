﻿namespace Mainspace.Models.Request
{
    public class SaveDimensionGroupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
