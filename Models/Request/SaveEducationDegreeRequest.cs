﻿namespace Mainspace.Models.Request
{
    public class SaveEducationDegreeRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
