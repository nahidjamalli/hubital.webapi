﻿using Mainspace.Models.Response;
using System.Collections.Generic;

namespace Mainspace.Models.Request
{
    public class SaveEmployeeDemandRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string ResponsibilityNote { get; set; }
        public int GenderId { get; set; }
        public int EmployeeCount { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int MinExperience { get; set; }
        public int MaxExperience { get; set; }
        public int DepartmentId { get; set; }
        public int PositionId { get; set; }
        public int ContractTypeId { get; set; }
        public string ReasonNote { get; set; }
        public int[] EducationDegrees { get; set; }
        public int[] Skills { get; set; }
        public int ReasonId { get; set; }
        public int[] ApprovalEmployees { get; set; }
        public IEnumerable<LanguageArray> Languages { get; set; }
        public int[] Moderators { get; set; }
        public int StatusId { get; set; }
        public int StructureId { get; set; }
        public int CreatorUserId { get; set; }
    }
}
