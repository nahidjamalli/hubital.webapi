﻿namespace Mainspace.Models.Request
{
    public class SaveEmployeeRequest
    {
        public int Id { get; set; }
        public int JobOfferId { get; set; }
        public int StatusId { get; set; }
        public string StartDate { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string IDNumber { get; set; }
        public string SIN { get; set; }
        public string TIN { get; set; }
        public string PrimaryEmail { get; set; }
        public int? CitizenshipId { get; set; }
        public int? BirthCountryId { get; set; }
        public int? BirthCityId { get; set; }
        public string BirthDate { get; set; }
        public int GenderId { get; set; }
        public int? BloodGroupId { get; set; }
        public int? ReligionId { get; set; }
        public int EmploymentTypeId { get; set; }
        public int[] DrivingCategories { get; set; }
        public int? MilitaryStatusId { get; set; }
        public int? MaritalStatusId { get; set; }
        public int Experience { get; set; }
        public string Note { get; set; }
    }
}
