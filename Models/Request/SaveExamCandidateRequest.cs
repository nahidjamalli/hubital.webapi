﻿namespace Mainspace.Models.Request
{
    public class SaveExamCandidateRequest
    {
        public int Id { get; set; }
        public int ExamId { get; set; }
        public int CandidateId { get; set; }
        public bool IsEmployee { get; set; }
        public bool ShowResult { get; set; }
        public string Note { get; set; }
    }
}
