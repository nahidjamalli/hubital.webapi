﻿namespace Mainspace.Models.Request
{
    public class SaveExamJoinQuestionRequest
    {
        public int QuestionId { get; set; }
        public int ExamId { get; set; }
    }
}
