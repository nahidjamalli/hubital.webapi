﻿namespace Mainspace.Models.Request
{
    public class SaveExamJoinRuleRequest
    {
        public int RuleId { get; set; }
        public int ExamId { get; set; }
    }
}
