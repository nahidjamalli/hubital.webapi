﻿namespace Mainspace.Models.Request
{
    public class SaveExamRequest
    {
        public int Id { get; set; }
        public string LanguageId { get; set; }
        public int ExamTypeId { get; set; }
        public string Name { get; set; }
        public int RequiredScore { get; set; }
        public int Duration { get; set; }
        public string Note { get; set; }
    }
}
