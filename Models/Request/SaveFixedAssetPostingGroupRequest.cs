﻿namespace Mainspace.Models.Request
{
    public class SaveFixedAssetPostingGroupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int AcquisitionCostAccountId { get; set; }
    }
}
