﻿namespace Mainspace.Models.Request
{
    public class SaveGeneralPostingSetupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int GeneralBusinessPostingGroupId { get; set; }
        public int GeneralProductPostingGroupId { get; set; }
        public int SalesAccountId { get; set; }
        public int PurchaseAccountId { get; set; }
    }
}
