﻿namespace Mainspace.Models.Request
{
    public class SaveGeneralProductPostingGroupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
