﻿namespace Mainspace.Models.Request
{
    public class SaveGroupJoinPermissionRequest
    {
        public int GroupId { get; set; }
        public int PermissionId { get; set; }
    }
}
