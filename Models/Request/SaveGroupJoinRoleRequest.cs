﻿namespace Mainspace.Models.Request
{
    public class SaveGroupJoinRoleRequest
    {
        public int GroupId { get; set; }
        public int RoleId { get; set; }
    }
}
