﻿namespace Mainspace.Models.Request
{
    public class SaveGroupRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
    }
}
