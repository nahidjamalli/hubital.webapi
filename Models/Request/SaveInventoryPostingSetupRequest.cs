﻿namespace Mainspace.Models.Request
{
    public class SaveInventoryPostingSetupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int InventoryAccountId { get; set; }
        public int InventoryPostingGroupId { get; set; }
        public int LocationId { get; set; }
    }
}
