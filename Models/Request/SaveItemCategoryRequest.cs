﻿namespace Mainspace.Models.Request
{
    public class SaveItemCategoryRequest
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public string SearchName { get; set; }
        public string Note { get; set; }
    }
}
