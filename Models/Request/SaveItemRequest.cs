﻿namespace Mainspace.Models.Request
{
    public class SaveItemRequest
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int BaseUnitOfMeasureId { get; set; }
        public int ItemCategoryId { get; set; }
        public int ItemTypeId { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal UnitCost { get; set; }
    }
}
