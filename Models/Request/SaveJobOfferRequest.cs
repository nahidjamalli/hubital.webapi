﻿namespace Mainspace.Models.Request
{
    public class SaveJobOfferRequest
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public int CreatorUserId { get; set; }
        public int Amount { get; set; }
        public int StatusId { get; set; }
        public string OfferDate { get; set; }
        public string OfferExpirationDate { get; set; }
        public string Note { get; set; }
    }
}
