﻿namespace Mainspace.Models.Request
{
    public class SaveJournalLineJoinAccountAnalysisDimensionRequest
    {
        public int JournalLineId { get; set; }
        public int AccountId { get; set; }
        public int DimensionGroupId { get; set; }
        public int? AnalysisDimensionId { get; set; }
    }
}
