﻿using System;

namespace Mainspace.Models.Request
{
    public class SaveJournalLineRequest
    {
        public int Id { get; set; }
        public int JournalId { get; set; }
        public DateTimeOffset LineDate { get; set; }
        public DateTimeOffset? EntryExpirationDate { get; set; }
        public int? EntryTypeCode { get; set; }
        public int DebitAccountId { get; set; }
        public int CreditAccountId { get; set; }
        public decimal Amount { get; set; }
        public decimal PaidAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string Note { get; set; }
        public int ParentId { get; set; }
        public string DocumentNumber { get; set; }
    }
}
