﻿namespace Mainspace.Models.Request
{
    public class SaveJournalRequest
    {
        public int Id { get; set; }
        public int JournalTypeId { get; set; }
        public int JournalModeId { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public int StatusCode { get; set; }
    }
}
