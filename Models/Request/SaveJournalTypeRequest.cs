﻿namespace Mainspace.Models.Request
{
    public class SaveJournalTypeRequest
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
