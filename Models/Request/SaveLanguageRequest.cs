﻿namespace Mainspace.Models.Request
{
    public class SaveLanguageRequest
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
