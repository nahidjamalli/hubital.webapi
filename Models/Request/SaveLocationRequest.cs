﻿namespace Mainspace.Models.Request
{
    public class SaveLocationRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SearchName { get; set; }
    }
}
