﻿namespace Mainspace.Models.Request
{
    public class SavePermissionRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
