﻿namespace Mainspace.Models.Request
{
    public class SaveProbationPeriodJoinSkillsRequest
    {
        public int ProbationPeriodId { get; set; }
        public int SkillId { get; set; }
        public float Evaluation { get; set; }
    }
}
