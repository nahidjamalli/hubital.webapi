﻿namespace Mainspace.Models.Request
{
    public class SaveProbationPeriodRequest
    {
        public int Id { get; set; }
        public int CreatorUserId { get; set; }
        public int StatusId { get; set; }
        public int EmployeeId { get; set; }
        public int ApprovalEmployeeId { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public string Note { get; set; }
    }
}
