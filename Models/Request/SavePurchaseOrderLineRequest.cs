﻿namespace Mainspace.Models.Request
{
    public class SavePurchaseOrderLineRequest
    {
        public int Id { get; set; }
        public int PurchaseOrderId { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public int LocationId { get; set; }
    }
}
