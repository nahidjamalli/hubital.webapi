﻿namespace Mainspace.Models.Request
{
    public class SavePurchaseOrderRequest
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
    }
}
