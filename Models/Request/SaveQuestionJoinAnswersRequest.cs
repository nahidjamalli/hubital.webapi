﻿namespace Mainspace.Models.Request
{
    public class SaveQuestionJoinAnswersRequest
    {
        public int ExamHistoryId { get; set; }
        public int TimeLeft { get; set; }
        public int QuestionId { get; set; }
        public int[] Answers { get; set; }
        public string Answer { get; set; }
    }
}
