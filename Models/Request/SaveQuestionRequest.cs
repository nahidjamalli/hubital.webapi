﻿namespace Mainspace.Models.Request
{
    public class SaveQuestionRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int QuestionTypeId { get; set; }
        public int QuestionGroupId { get; set; }
        public int Duration { get; set; }
        public int Score { get; set; }
        public string Note { get; set; }
    }
}
