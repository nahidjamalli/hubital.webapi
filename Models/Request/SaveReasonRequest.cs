﻿namespace Mainspace.Models.Request
{
    public class SaveReasonRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
