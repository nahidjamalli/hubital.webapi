﻿namespace Mainspace.Models.Request
{
    public class SaveRoleJoinPermissionRequest
    {
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
    }
}
