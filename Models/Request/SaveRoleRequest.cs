﻿namespace Mainspace.Models.Request
{
    public class SaveRoleRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
