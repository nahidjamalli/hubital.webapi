﻿namespace Mainspace.Models.Request
{
    public class SaveSkillRequest
    {
        public int Id { get; set; }
        public int SkillTypeId { get; set; }
        public string Name { get; set; }
    }
}
