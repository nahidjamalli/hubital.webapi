﻿namespace Mainspace.Models.Request
{
    public class SaveSkillTypeRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
