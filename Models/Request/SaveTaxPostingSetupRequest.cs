﻿namespace Mainspace.Models.Request
{
    public class SaveTaxPostingSetupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int TaxBusinessPostingGroupId { get; set; }
        public int TaxProductPostingGroupId { get; set; }
        public decimal TaxPercent { get; set; }
        public int SalesTaxAccountId { get; set; }
        public int PurchaseTaxAccountId { get; set; }
    }
}
