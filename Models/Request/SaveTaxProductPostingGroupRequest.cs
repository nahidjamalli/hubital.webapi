﻿namespace Mainspace.Models.Request
{
    public class SaveTaxProductPostingGroupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
