﻿namespace Mainspace.Models.Request
{
    public class SaveUnitOfMeasureRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
    }
}
