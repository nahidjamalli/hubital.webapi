﻿namespace Mainspace.Models.Request
{
    public class SaveUserJoinPermissionRequest
    {
        public int UserId { get; set; }
        public int PermissionId { get; set; }
    }
}
