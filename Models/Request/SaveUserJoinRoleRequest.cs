﻿namespace Mainspace.Models.Request
{
    public class SaveUserJoinRoleRequest
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
