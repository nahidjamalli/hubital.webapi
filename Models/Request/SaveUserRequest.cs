﻿namespace Mainspace.Models.Request
{
    public class SaveUserRequest
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int StatusId { get; set; }
        public int EmployeeId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Note { get; set; }
    }
}
