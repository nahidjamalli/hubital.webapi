﻿namespace Mainspace.Models.Request
{
    public class SaveVendorPostingGroupRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int PayablesAccountId { get; set; }
    }
}
