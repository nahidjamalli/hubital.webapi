﻿namespace Mainspace.Models.Request
{
    public class SaveVendorRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SearchName { get; set; }
        public string Note { get; set; }
        public VendorJoinContacts[] Contacts { get; set; }
        public VendorJoinAddresses[] Addresses { get; set; }

    }

    public class VendorJoinContacts
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int ContactTypeId { get; set; }
        public string Contact { get; set; }
    }

    public class VendorJoinAddresses
    {
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int AddressTypeId { get; set; }
        public string Address { get; set; }
    }
}
