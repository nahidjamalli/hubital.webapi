﻿namespace Mainspace.Models.Request
{
    public class SaveZoneRequest
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int BinTypeId { get; set; }
    }
}
