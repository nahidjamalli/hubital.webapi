﻿namespace Mainspace.Models.Request
{
    public class StartExamRequest
    {
        public string Code { get; set; }
    }
}
