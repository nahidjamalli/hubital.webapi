﻿namespace Mainspace.Models.Request
{
    public class UpdateExamJoinQuestionRequest
    {
        public int Id { get; set; }
        public byte Order { get; set; }
    }
}
