﻿namespace Mainspace.Models.Response
{
    public class AppointmentHistoryArray
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string InterviewType { get; set; }
        public string Decision { get; set; }
        public string FromDateTime { get; set; }
        public string ToDateTime { get; set; }
        public string Note { get; set; }
    }
}
