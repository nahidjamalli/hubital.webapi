﻿namespace Mainspace.Models.Response
{
    public class ContactArray
    {
        public int? Id { get; set; }
        public int ContactTypeId { get; set; }
        public string Contact { get; set; }
    }
}
