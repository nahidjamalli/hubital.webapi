﻿namespace Mainspace.Models.Response
{
    public class DimensionGroupPostTypeArray
    {
        public int Id { get; set; }
        public int PostType { get; set; }
    }
}
