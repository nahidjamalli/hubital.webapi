﻿using System.Collections.Generic;

namespace Mainspace.Models.Response
{
    public class FileArray
    {
        public bool success { get; set; }
        public object errorId { get; set; }
        public List<Result> result { get; set; }
    }

    public class Result
    {
        public string url { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public System.DateTime dateModified { get; set; }
        public bool isDirectory { get; set; }
        public long size { get; set; }
        public bool hasSubDirectories { get; set; }
    }
}
