﻿namespace Mainspace.Models.Response
{
    public class GetMailEmployeeDemandJoinApprovalEmployeesResponse
    {
        public int Id { get; set; }
        public string CreatorUserFullName { get; set; }
        public string Department { get; set; }
        public string ReasonNote { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
    }
}
