﻿namespace Mainspace.Models.Response
{
    public class GetMailTemplateResponse
    {
        public string Content { get; set; }
    }
}
