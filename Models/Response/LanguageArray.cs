﻿namespace Mainspace.Models.Response
{
    public class LanguageArray
    {
        public string LanguageId { get; set; }
        public bool Reading { get; set; }
        public bool Writing { get; set; }
        public bool Speaking { get; set; }
        public bool Listening { get; set; }
    }
}
