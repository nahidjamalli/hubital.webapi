using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Mainspace
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost("http://*:5800").Run();
        }

        private static IWebHost BuildWebHost(string port)
        {
            return WebHost.CreateDefaultBuilder()
                .UseUrls(port)
                .UseStartup<Startup>()
                .Build();
        }
    }
}
