﻿using Dapper;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class AdministrationRepository
    {
        readonly DBClient _dbClient;
        public AdministrationRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region Groups
        public async Task<object> SaveGroup(int id, string name, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("admin.SaveGroup", p);
        }

        public async Task<object> GetGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.GetGroup", p);
        }

        public async Task<object> GetGroups()
        {
            return await _dbClient.Query<object>("admin.GetGroups");
        }

        public async Task<object> DeleteGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteGroup", p);
        }

        public async Task<object> GetGroupJoinPermissions(int groupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@groupId", groupId);
            return await _dbClient.Query<object>("admin.GetGroupJoinPermissions", p);
        }

        public async Task<object> GetGroupJoinRoles(int groupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@groupId", groupId);
            return await _dbClient.Query<object>("admin.GetGroupJoinRoles", p);
        }

        public async Task<object> SaveGroupJoinRole(int groupId, int roleId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@groupId", groupId);
            p.Add("@roleId", roleId);
            return await _dbClient.QuerySingle<object>("admin.SaveGroupJoinRole", p);
        }

        public async Task<object> SaveGroupJoinPermission(int groupId, int permissionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@groupId", groupId);
            p.Add("@permissionId", permissionId);
            return await _dbClient.QuerySingle<object>("admin.SaveGroupJoinPermission", p);
        }

        public async Task<object> DeleteGroupJoinRole(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteGroupJoinRole", p);
        }

        public async Task<object> DeleteGroupJoinPermission(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteGroupJoinPermission", p);
        }
        #endregion

        #region Users
        public async Task<object> SaveUser(int id, int groupId, int statusId, int employeeId, string username, string password, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@groupId", groupId);
            p.Add("@statusId", statusId);
            p.Add("@employeeId", employeeId);
            p.Add("@username", username);
            p.Add("@password", password);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("admin.SaveUser", p);
        }

        public async Task<object> GetUser(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.GetUser", p);
        }

        public async Task<object> GetUsers()
        {
            return await _dbClient.Query<object>("admin.GetUsers");
        }

        public async Task<object> DeleteUser(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteUser", p);
        }

        public async Task<object> SignIn(string username, string password)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@username", username);
            p.Add("@password", password);
            return await _dbClient.QuerySingle<object>("admin.SignIn", p);
        }

        public async Task<object> GetUserJoinPermissions(int userId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            return await _dbClient.Query<object>("admin.GetUserJoinPermissions", p);
        }

        public async Task<object> GetCurrentUserJoinPermissions(int userId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            return await _dbClient.Query<object>("admin.GetCurrentUserJoinPermissions", p);
        }

        public async Task<object> GetUserJoinRoles(int userId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            return await _dbClient.Query<object>("admin.GetUserJoinRoles", p);
        }

        public async Task<object> SaveUserJoinRole(int userId, int roleId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@roleId", roleId);
            return await _dbClient.QuerySingle<object>("admin.SaveUserJoinRole", p);
        }

        public async Task<object> SaveUserJoinPermission(int userId, int permissionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@permissionId", permissionId);
            return await _dbClient.QuerySingle<object>("admin.SaveUserJoinPermission", p);
        }

        public async Task<object> DeleteUserJoinRole(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteUserJoinRole", p);
        }

        public async Task<object> DeleteUserJoinPermission(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteUserJoinPermission", p);
        }

        public async Task<object> GetUserJoinEmployee(int userId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            return await _dbClient.QuerySingle<object>("admin.GetUserJoinEmployee", p);
        }
        #endregion

        #region Roles
        public async Task<object> SaveRole(int id, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("admin.SaveRole", p);
        }

        public async Task<object> GetRole(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.GetRole", p);
        }

        public async Task<object> GetRoles(string code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.Query<object>("admin.GetRoles", p);
        }

        public async Task<object> DeleteRole(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteRole", p);
        }

        public async Task<object> GetRoleJoinPermissions(int roleId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@roleId", roleId);
            return await _dbClient.Query<object>("admin.GetRoleJoinPermissions", p);
        }

        public async Task<object> SaveRoleJoinPermission(int roleId, int permissionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@roleId", roleId);
            p.Add("@permissionId", permissionId);
            return await _dbClient.QuerySingle<object>("admin.SaveRoleJoinPermission", p);
        }

        public async Task<object> DeleteRoleJoinPermission(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeleteRoleJoinPermission", p);
        }
        #endregion

        #region Permissions
        public async Task<object> SavePermission(int id, string code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("admin.SavePermission", p);
        }

        public async Task<object> GetPermission(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.GetPermission", p);
        }

        public async Task<object> GetPermissions(string code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.Query<object>("admin.GetPermissions", p);
        }

        public async Task<object> DeletePermission(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("admin.DeletePermission", p);
        }
        #endregion

        #region Notifications
        public async Task<object> GetNotificationsCount(int userId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            return await _dbClient.QuerySingle<object>("admin.GetNotificationsCount", p);
        }

        public async Task<object> MarkAsReadNotification(int userId, int refId, int notificationTypeId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@refId", refId);
            p.Add("@notificationTypeId", notificationTypeId);
            return await _dbClient.QuerySingle<object>("admin.MarkAsReadNotification", p);
        }
        #endregion
    }
}
