﻿using Dapper;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class ConfigurationRepository
    {
        readonly DBClient _dbClient;
        public ConfigurationRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region Companies
        public async Task<object> GetCompanies()
        {
            return await _dbClient.Query<object>("config.GetCompanies");
        }

        public async Task<object> DeleteCompany(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("config.DeleteCompany", p);
        }

        public async Task<object> SaveCompany(int id, string name, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("config.SaveCompany", p);
        }
        #endregion

        #region Statuses
        public async Task<object> GetStatuses(int userId, int statusTypeId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@statusTypeId", statusTypeId);
            return await _dbClient.Query<object>("config.GetStatuses", p);
        }
        #endregion

        #region Languages
        public async Task<object> GetLanguages()
        {
            return await _dbClient.Query<object>("config.GetLanguages");
        }

        public async Task<object> DeleteLanguage(string id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("config.DeleteLanguage", p);
        }

        public async Task<object> SaveLanguage(string id, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("config.SaveLanguage", p);
        }
        #endregion

        #region Countries
        public async Task<object> GetCountries()
        {
            return await _dbClient.Query<object>("config.GetCountries");
        }
        #endregion

        #region Cities
        public async Task<object> GetCities(int countryId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@countryId", countryId);
            return await _dbClient.Query<object>("config.GetCities", p);
        }
        #endregion

        #region ContactTypes
        public async Task<object> GetContactTypes()
        {
            return await _dbClient.Query<object>("config.GetContactTypes");
        }
        #endregion

        #region AddressTypes
        public async Task<object> GetAddressTypes()
        {
            return await _dbClient.Query<object>("config.GetAddressTypes");
        }
        #endregion

        #region BankAccountPostingGroups
        public async Task<object> GetBankAccountPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetBankAccountPostingGroups");
        }

        public async Task<object> SaveBankAccountPostingGroup(int id, string code, string name, int bankAccountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@bankAccountId", bankAccountId);
            return await _dbClient.QuerySingle<object>("config.SaveBankAccountPostingGroup", p);
        }

        public async Task<object> DeleteBankAccountPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteBankAccountPostingGroup", p);
        }
        #endregion

        #region CustomerPostingGroups
        public async Task<object> GetCustomerPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetCustomerPostingGroups");
        }

        public async Task<object> SaveCustomerPostingGroup(int id, string code, string name, int receivablesAccountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@receivablesAccountId", receivablesAccountId);
            return await _dbClient.QuerySingle<object>("config.SaveCustomerPostingGroup", p);
        }

        public async Task<object> DeleteCustomerPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteCustomerPostingGroup", p);
        }
        #endregion

        #region FixedAssetPostingGroups
        public async Task<object> GetFixedAssetPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetFixedAssetPostingGroups");
        }

        public async Task<object> SaveFixedAssetPostingGroup(int id, string code, string name, int acquisitionCostAccountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@acquisitionCostAccountId", acquisitionCostAccountId);
            return await _dbClient.QuerySingle<object>("config.SaveFixedAssetPostingGroup", p);
        }

        public async Task<object> DeleteFixedAssetPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteFixedAssetPostingGroup", p);
        }
        #endregion

        #region GeneralBusinessPostingGroups
        public async Task<object> GetGeneralBusinessPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetGeneralBusinessPostingGroups");
        }

        public async Task<object> SaveGeneralBusinessPostingGroup(int id, string code, string description)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            return await _dbClient.QuerySingle<object>("config.SaveGeneralBusinessPostingGroup", p);
        }

        public async Task<object> DeleteGeneralBusinessPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteGeneralBusinessPostingGroup", p);
        }
        #endregion

        #region GeneralProductPostingGroups
        public async Task<object> GetGeneralProductPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetGeneralProductPostingGroups");
        }

        public async Task<object> SaveGeneralProductPostingGroup(int id, string code, string description)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            return await _dbClient.QuerySingle<object>("config.SaveGeneralProductPostingGroup", p);
        }

        public async Task<object> DeleteGeneralProductPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteGeneralProductPostingGroup", p);
        }
        #endregion

        #region GeneralPostingSetups
        public async Task<object> GetGeneralPostingSetups()
        {
            return await _dbClient.Query<object>("config.GetGeneralPostingSetups");
        }

        public async Task<object> SaveGeneralPostingSetup(int id, string code, int generalBusinessPostingGroupId, int generalProductPostingGroupId, int salesAccountId, int purchaseAccountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@generalBusinessPostingGroupId", generalBusinessPostingGroupId);
            p.Add("@generalProductPostingGroupId", generalProductPostingGroupId);
            p.Add("@salesAccountId", salesAccountId);
            p.Add("@purchaseAccountId", purchaseAccountId);
            return await _dbClient.QuerySingle<object>("config.SaveGeneralPostingSetup", p);
        }

        public async Task<object> DeleteGeneralPostingSetup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteGeneralPostingSetup", p);
        }
        #endregion

        #region InventoryPostingSetups
        public async Task<object> GetInventoryPostingSetups()
        {
            return await _dbClient.Query<object>("config.GetInventoryPostingSetups");
        }

        public async Task<object> SaveInventoryPostingSetup(int id, string code, string name, int inventoryAccountId, int inventoryPostingGroupId, int locationId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@inventoryAccountId", inventoryAccountId);
            p.Add("@inventoryPostingGroupId", inventoryPostingGroupId);
            p.Add("@locationId", locationId);
            return await _dbClient.QuerySingle<object>("config.SaveInventoryPostingSetup", p);
        }

        public async Task<object> DeleteInventoryPostingSetup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteInventoryPostingSetup", p);
        }
        #endregion

        #region InventoryPostingGroups
        public async Task<object> GetInventoryPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetInventoryPostingGroups");
        }

        public async Task<object> SaveInventoryPostingGroup(int id, string code, string description)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            return await _dbClient.QuerySingle<object>("config.SaveInventoryPostingGroup", p);
        }

        public async Task<object> DeleteInventoryPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteInventoryPostingGroup", p);
        }
        #endregion

        #region TaxPostingSetups
        public async Task<object> GetTaxPostingSetups()
        {
            return await _dbClient.Query<object>("config.GetTaxPostingSetups");
        }

        public async Task<object> SaveTaxPostingSetup(int id, string code, string description, int taxBusinessPostingGroupId, int taxProductPostingGroupId, decimal taxPercent, int salesTaxAccountId, int purchaseTaxAccountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            p.Add("@taxBusinessPostingGroupId", taxBusinessPostingGroupId);
            p.Add("@taxProductPostingGroupId", taxProductPostingGroupId);
            p.Add("@taxPercent", taxPercent);
            p.Add("@salesTaxAccountId", salesTaxAccountId);
            p.Add("@purchaseTaxAccountId", purchaseTaxAccountId);
            return await _dbClient.QuerySingle<object>("config.SaveTaxPostingSetup", p);
        }

        public async Task<object> DeleteTaxPostingSetup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteTaxPostingSetup", p);
        }
        #endregion

        #region TaxBusinessPostingGroups
        public async Task<object> GetTaxBusinessPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetTaxBusinessPostingGroups");
        }

        public async Task<object> SaveTaxBusinessPostingGroup(int id, string code, string description)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            return await _dbClient.QuerySingle<object>("config.SaveTaxBusinessPostingGroup", p);
        }

        public async Task<object> DeleteTaxBusinessPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteTaxBusinessPostingGroup", p);
        }
        #endregion

        #region TaxProductPostingGroups
        public async Task<object> GetTaxProductPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetTaxProductPostingGroups");
        }

        public async Task<object> SaveTaxProductPostingGroup(int id, string code, string description)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            return await _dbClient.QuerySingle<object>("config.SaveTaxProductPostingGroup", p);
        }

        public async Task<object> DeleteTaxProductPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteTaxProductPostingGroup", p);
        }
        #endregion

        #region VendorPostingGroups
        public async Task<object> GetVendorPostingGroups()
        {
            return await _dbClient.Query<object>("config.GetVendorPostingGroups");
        }

        public async Task<object> SaveVendorPostingGroup(int id, string code, string description, int payablesAccountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            p.Add("@payablesAccountId", payablesAccountId);
            return await _dbClient.QuerySingle<object>("config.SaveVendorPostingGroups", p);
        }

        public async Task<object> DeleteVendorPostingGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("config.DeleteVendorPostingGroups", p);
        }
        #endregion
    }
}
