﻿using Dapper;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class ContractManagementRepository
    {
        readonly DBClient _dbClient;
        public ContractManagementRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region ContractTypes
        public async Task<object> SaveContractType(int id, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("contract.SaveContractType", p);
        }

        public async Task<object> GetContractTypes()
        {
            return await _dbClient.Query<object>("contract.GetContractTypes");
        }

        public async Task<object> DeleteContractType(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("contract.DeleteContractType", p);
        }
        #endregion
    }
}
