﻿using Dapper;
using Mainspace.Models.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class DBClient
    {
        readonly SQLConnection _sqlConn;

        public DBClient(SQLConnection sqlConn)
        {
            _sqlConn = sqlConn;
        }

        public async Task<IEnumerable<T>> Query<T>(string procedure, object parameter = null, CommandType commandType = CommandType.StoredProcedure)
        {
            SqlConnection connection = new SqlConnection(_sqlConn.ConnectionString);
            await connection.OpenAsync();
            var result = await connection.QueryAsync<T>(procedure, parameter, commandType: commandType);
            await connection.CloseAsync();
            return result;
        }

        public async Task<T> QuerySingle<T>(string procedure, object parameter, CommandType commandType = CommandType.StoredProcedure)
        {
            SqlConnection connection = new SqlConnection(_sqlConn.ConnectionString);
            await connection.OpenAsync();
            var result = await connection.QuerySingleOrDefaultAsync<T>(procedure, parameter, commandType: commandType);
            await connection.CloseAsync();
            return result;
        }
    }
}
