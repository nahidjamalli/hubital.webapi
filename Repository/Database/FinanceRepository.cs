﻿using Dapper;
using Mainspace.Models.Response;
using Mainspace.Repository.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class FinanceRepository
    {
        readonly DBClient _dbClient;
        public FinanceRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region Banks
        public async Task<object> GetBanks()
        {
            return await _dbClient.Query<object>("finance.GetBanks");
        }

        public async Task<object> SaveBank(int id, string code, string fullName, string shortName, string swiftCode, string email, string website, string address, string contact, string correspondentAccountCode, string tin, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@fullName", fullName);
            p.Add("@shortName", shortName);
            p.Add("@swiftCode", swiftCode);
            p.Add("@email", email);
            p.Add("@website", website);
            p.Add("@address", address);
            p.Add("@contact", contact);
            p.Add("@correspondentAccountCode", correspondentAccountCode);
            p.Add("@tin", tin);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("finance.SaveBank", p);
        }

        public async Task<object> DeleteBank(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteBank", p);
        }
        #endregion

        #region BankAccounts
        public async Task<object> GetBankAccounts()
        {
            return await _dbClient.Query<object>("finance.GetBankAccounts");
        }

        public async Task<object> SaveBankAccount(int id, int bankId, string bankAccountNumber, string currencyCode, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@bankId", bankId);
            p.Add("@bankAccountNumber", bankAccountNumber);
            p.Add("@currencyCode", currencyCode);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("finance.SaveBankAccount", p);
        }

        public async Task<object> DeleteBankAccount(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteBankAccount", p);
        }
        #endregion

        #region AnalysisSections
        public async Task<object> SaveAnalysisSectionJoinDimensionGroup(int analysisSectionCode, int dimensionGroupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@analysisSectionCode", analysisSectionCode);
            p.Add("@dimensionGroupId", dimensionGroupId);
            return await _dbClient.QuerySingle<object>("finance.SaveAnalysisSectionJoinDimensionGroup", p);
        }

        public async Task<object> DeleteAnalysisSectionJoinDimensionGroup(int analysisSectionCode, int dimensionGroupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@analysisSectionCode", analysisSectionCode);
            p.Add("@dimensionGroupId", dimensionGroupId);
            return await _dbClient.QuerySingle<object>("finance.DeleteAnalysisSectionJoinDimensionGroup", p);
        }

        public async Task<object> GetAnalysisSections()
        {
            return await _dbClient.Query<object>("finance.GetAnalysisSections");
        }

        public async Task<object> GetAnalysisSectionJoinDimensionGroups(int analysisSectionCode)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@analysisSectionCode", analysisSectionCode);
            return await _dbClient.Query<object>("finance.GetAnalysisSectionJoinDimensionGroups", p);
        }

        public async Task<object> GetAnalysisSection(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.GetAnalysisSection", p);
        }
        #endregion

        #region DimensionGroups
        public async Task<object> SaveDimensionGroup(int id, string code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("finance.SaveDimensionGroup", p);
        }

        public async Task<object> DeleteDimensionGroup(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteDimensionGroup", p);
        }

        public async Task<object> GetDimensionGroups()
        {
            return await _dbClient.Query<object>("finance.GetDimensionGroups");
        }
        #endregion

        #region AnalysisDimensions
        public async Task<object> SaveAnalysisDimension(int id, int parentId, int dimensionGroupId, string code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@parentId", parentId);
            p.Add("@dimensionGroupId", dimensionGroupId);
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("finance.SaveAnalysisDimension", p);
        }

        public async Task<object> DeleteAnalysisDimension(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteAnalysisDimension", p);
        }

        public async Task<object> GetAnalysisDimension(int analysisDimensionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@analysisDimensionId", analysisDimensionId);
            return await _dbClient.QuerySingle<object>("finance.GetAnalysisDimension", p);
        }

        public async Task<object> GetAnalysisDimensions(int dimensionGroupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@dimensionGroupId", dimensionGroupId);
            return await _dbClient.Query<object>("finance.GetAnalysisDimensions", p);
        }
        #endregion

        #region BalanceTypes
        public async Task<object> GetBalanceTypes()
        {
            return await _dbClient.Query<object>("finance.GetBalanceTypes");
        }
        #endregion

        #region AccountCategories
        public async Task<object> GetAccountCategories(string code, string name, int pageNumber, int rowCount)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@pageNumber", 0);
            p.Add("@rowCount", 500);
            return await _dbClient.Query<object>("finance.GetAccountCategories", p);
        }

        public async Task<object> GetAccountCategory(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.GetAccountCategory", p);
        }

        public async Task<object> SaveAccountCategory(int id, int parentId, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@parentId", parentId);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("finance.SaveAccountCategory", p);
        }

        public async Task<object> DeleteAccountCategory(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteAccountCategory", p);
        }

        public async Task<object> GetAccountCategoryParents()
        {
            return await _dbClient.Query<object>("finance.GetAccountCategoryParents");
        }
        #endregion

        #region Accounts
        public async Task<object> GetAccountParents()
        {
            return await _dbClient.Query<object>("finance.GetAccountParents");
        }

        public async Task<object> GetAccounts()
        {
            return await _dbClient.Query<object>("finance.GetAccounts");
        }

        public async Task<object> GetAccountJoinDimensionGroups(int accountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@accountId", accountId);
            return await _dbClient.Query<object>("finance.GetAccountJoinDimensionGroups", p);
        }

        public async Task<object> GetAccount(int accountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@accountId", accountId);
            return await _dbClient.QuerySingle<object>("finance.GetAccount", p);
        }

        public async Task<object> SaveAccount(int accountId, int parentId, string code, string mapCode, string name, int statusCode, int accountGroupCode, int accountCategoryId, int transactionTypeCode, int balanceTypeCode, int accountTypeCode, List<DimensionGroupPostTypeArray> dimensionGroups)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@accountId", accountId);
            p.Add("@parentId", parentId);
            p.Add("@code", code);
            p.Add("@mapCode", mapCode);
            p.Add("@name", name);
            p.Add("@statusCode", statusCode);
            p.Add("@accountGroupCode", accountGroupCode);
            p.Add("@accountCategoryId", accountCategoryId);
            p.Add("@transactionTypeCode", transactionTypeCode);
            p.Add("@balanceTypeCode", balanceTypeCode);
            p.Add("@accountTypeCode", accountTypeCode);
            p.Add("@dimensionGroups", dimensionGroups.AsTableValuedParameters("eland.DimensionGroupPostTypeArray"));
            return await _dbClient.QuerySingle<object>("finance.SaveAccount", p);
        }

        public async Task<object> DeleteAccount(int accountId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@accountId", accountId);
            return await _dbClient.QuerySingle<object>("finance.DeleteAccount", p);
        }

        public async Task<object> SaveAccountJoinAnalysisDimension(int accountId, int dimensionGroupId, int analysisDimensionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@accountId", accountId);
            p.Add("@dimensionGroupId", dimensionGroupId);
            p.Add("@analysisDimensionId", analysisDimensionId);
            return await _dbClient.QuerySingle<object>("finance.SaveAccountJoinAnalysisDimension", p);
        }
        #endregion

        #region TransactionTypes
        public async Task<object> GetTransactionTypes()
        {
            return await _dbClient.Query<object>("finance.GetTransactionTypes");
        }
        #endregion

        #region ElectronicInvoiceCodes
        public async Task<object> GetElectronicInvoiceCodes(string code, string name, int pageNumber, int rowCount)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@pageNumber", pageNumber);
            p.Add("@rowCount", rowCount);
            return await _dbClient.Query<object>("finance.GetElectronicInvoiceCodes", p);
        }
        #endregion

        #region DimensionPostTypes
        public async Task<object> GetDimensionPostTypes()
        {
            return await _dbClient.Query<object>("finance.GetDimensionPostTypes");
        }
        #endregion

        #region JournalTypes
        public async Task<object> GetJournalTypes()
        {
            return await _dbClient.Query<object>("finance.GetJournalTypes");
        }

        public async Task<object> SaveJournalType(int id, int code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("finance.SaveJournalType", p);
        }

        public async Task<object> DeleteJournalType(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteJournalType", p);
        }
        #endregion

        #region Journals
        public async Task<object> GetJournals()
        {
            return await _dbClient.Query<object>("finance.GetJournals");
        }

        public async Task<object> GetJournal(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.GetJournal", p);
        }

        public async Task<object> SaveJournal(int id, int journalTypeId, int journalModeId, string name, string note, int statusCode)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@journalTypeId", journalTypeId);
            p.Add("@journalModeId", journalModeId);
            p.Add("@name", name);
            p.Add("@note", note);
            p.Add("@statusCode", statusCode);
            return await _dbClient.QuerySingle<object>("finance.SaveJournal", p);
        }

        public async Task<object> DeleteJournal(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteJournal", p);
        }

        public async Task<object> GetJournalLines(int journalId, int journalModeId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@journalId", journalId);
            p.Add("@journalModeId", journalModeId);
            return await _dbClient.Query<object>("finance.GetJournalLines", p);
        }

        public async Task<object> SaveJournalLine(int id, int journalId, DateTimeOffset lineDate, DateTimeOffset? entryExpirationDate, int? entryTypeCode, int debitAccountId, int creditAccountId, decimal amount, decimal paidAmount, string currencyCode, string note, int parentId, string documentNumber)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@journalId", journalId);
            p.Add("@lineDate", lineDate);
            p.Add("@expirationDate", entryExpirationDate);
            p.Add("@entryTypeCode", entryTypeCode);
            p.Add("@debitAccountId", debitAccountId);
            p.Add("@creditAccountId", creditAccountId);
            p.Add("@amount", amount);
            p.Add("@paidAmount", paidAmount);
            p.Add("@currencyCode", currencyCode);
            p.Add("@note", note);
            p.Add("@parentId", parentId);
            p.Add("@documentNumber", documentNumber);
            return await _dbClient.QuerySingle<object>("finance.SaveJournalLine", p);
        }

        public async Task<object> SaveJournalLineJoinAccountAnalysisDimension(int journalLineId, int accountId, int dimensionGroupId, int? analysisDimensionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@journalLineId", journalLineId);
            p.Add("@accountId", accountId);
            p.Add("@dimensionGroupId", dimensionGroupId);
            p.Add("@analysisDimensionId", analysisDimensionId);
            return await _dbClient.QuerySingle<object>("finance.SaveJournalLineJoinAccountAnalysisDimension", p);
        }

        public async Task<object> DeleteJournalLine(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("finance.DeleteJournalLine", p);
        }

        public async Task<object> GetJournalModes()
        {
            return await _dbClient.Query<object>("finance.GetJournalModes");
        }

        public async Task<object> GetJournalLineJoinAccountAnalysisDimension(int journalLineId, int accountId, int dimensionGroupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@journalLineId", journalLineId);
            p.Add("@accountId", accountId);
            p.Add("@dimensionGroupId", dimensionGroupId);
            return await _dbClient.QuerySingle<object>("finance.GetJournalLineJoinAccountAnalysisDimension", p);
        }
        #endregion

        #region Currencies
        public async Task<object> GetCurrencies()
        {
            return await _dbClient.Query<object>("finance.GetCurrencies");
        }
        #endregion

        #region EntryTypes
        public async Task<object> GetEntryTypes()
        {
            return await _dbClient.Query<object>("finance.GetEntryTypes");
        }
        #endregion

        #region AccountTypes
        public async Task<object> GetAccountTypes()
        {
            return await _dbClient.Query<object>("finance.GetAccountTypes");
        }
        #endregion

        #region AccountGroups
        public async Task<object> GetAccountGroups()
        {
            return await _dbClient.Query<object>("finance.GetAccountGroups");
        }
        #endregion
    }
}
