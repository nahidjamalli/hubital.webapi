﻿using Dapper;
using Mainspace.Models.Response;
using Mainspace.Repository.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class HumanResourcesRepository
    {
        readonly DBClient _dbClient;
        public HumanResourcesRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region Structures
        public async Task<object> SaveStructure(int id, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("human.SaveStructure", p);
        }

        public async Task<object> DeleteStructure(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteStructure", p);
        }

        public async Task<object> GetStructures()
        {
            return await _dbClient.Query<object>("human.GetStructures");
        }
        #endregion

        #region Departments
        public async Task<object> SaveDepartment(int id, int parentId, int structureId, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@parentId", parentId);
            p.Add("@structureId", structureId);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("human.SaveDepartment", p);
        }

        public async Task<object> DeleteDepartment(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("human.DeleteDepartment", p);
        }

        public async Task<object> GetDepartments(int? structureId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@structureId", structureId);
            return await _dbClient.Query<object>("human.GetDepartments", p);
        }
        #endregion

        #region Positions
        public async Task<object> SavePosition(int id, int parentId, int structureId, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@parentId", parentId);
            p.Add("@structureId", structureId);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("human.SavePosition", p);
        }

        public async Task<object> DeletePosition(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("human.DeletePosition", p);
        }

        public async Task<object> GetPositions(int? structureId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@structureId", structureId);
            return await _dbClient.Query<object>("human.GetPositions", p);
        }
        #endregion

        #region Genders
        public async Task<object> GetGenders()
        {
            return await _dbClient.Query<object>("human.GetGenders");
        }
        #endregion

        #region EducationDegrees
        public async Task<object> GetEducationDegrees()
        {
            return await _dbClient.Query<object>("human.GetEducationDegrees");
        }

        public async Task<object> SaveEducationDegree(int id, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("human.SaveEducationDegree", p);
        }

        public async Task<object> DeleteEducationDegree(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("human.DeleteEducationDegree", p);
        }
        #endregion

        #region SkillTypes
        public async Task<object> GetSkillTypes()
        {
            return await _dbClient.Query<object>("human.GetSkillTypes");
        }

        public async Task<object> DeleteSkillType(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("human.DeleteSkillType", p);
        }

        public async Task<object> SaveSkillType(int id, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("human.SaveSkillType", p);
        }
        #endregion

        #region Skills
        public async Task<object> GetSkills(int skillTypeCode)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@skillTypeCode", skillTypeCode);
            return await _dbClient.Query<object>("human.GetSkills", p);
        }

        public async Task<object> DeleteSkill(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("human.DeleteSkill", p);
        }

        public async Task<object> SaveSkill(int id, int skillTypeId, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@skillTypeId", skillTypeId);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("human.SaveSkill", p);
        }
        #endregion

        #region Reasons
        public async Task<object> GetReasons()
        {
            return await _dbClient.Query<object>("human.GetReasons");
        }

        public async Task<object> SaveReason(int id, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("human.SaveReason", p);
        }

        public async Task<object> DeleteReason(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("human.DeleteReason", p);
        }
        #endregion

        #region EmployeeDemands
        public async Task<SaveEmployeeDemandResponse> SaveEmployeeDemand(int id, string name, string note, string responsibilityNote, int genderId, int employeeCount, int minAge, int maxAge, int minExperience, int maxExperience, int departmentId, int positionId, int contractTypeId, string reasonNote, int[] educationDegrees, int[] skills, int reasonId, int[] approvalEmployees, IEnumerable<LanguageArray> languages, int[] moderators, int statusId, int structureId, int creatorUserId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@note", note);
            p.Add("@responsibilityNote", responsibilityNote);
            p.Add("@genderId", genderId);
            p.Add("@employeeCount", employeeCount);
            p.Add("@minAge", minAge);
            p.Add("@maxAge", maxAge);
            p.Add("@minExperience", minExperience);
            p.Add("@maxExperience", maxExperience);
            p.Add("@departmentId", departmentId);
            p.Add("@positionId", positionId);
            p.Add("@contractTypeId", contractTypeId);
            p.Add("@reasonNote", reasonNote);
            p.Add("@educationDegrees", educationDegrees.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@skills", skills.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@reasonId", reasonId);
            p.Add("@approvalEmployees", approvalEmployees.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@languages", languages.AsTableValuedParameters("eland.Language"));
            p.Add("@moderators", moderators.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@statusId", statusId);
            p.Add("@structureId", structureId);
            p.Add("@creatorUserId", creatorUserId);
            return await _dbClient.QuerySingle<SaveEmployeeDemandResponse>("human.SaveEmployeeDemand", p);
        }

        public async Task<object> DeleteEmployeeDemand(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteEmployeeDemand", p);
        }

        public async Task<object> GetEmployeeDemands(int userId, string code, string name, string department, string position)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@department", department);
            p.Add("@position", position);
            return await _dbClient.Query<object>("human.GetEmployeeDemands", p);
        }

        public async Task<object> GetEmployeeDemand(int id, int? userId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@userId", userId);
            return await _dbClient.QuerySingle<object>("human.GetEmployeeDemand", p);
        }

        public async Task<object> IsReadOnlyEmployeeDemand(int userId, int employeeDemandId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@employeeDemandId", employeeDemandId);
            return await _dbClient.QuerySingle<object>("human.IsReadOnlyEmployeeDemand", p);
        }

        public async Task<object> GetEmployeeDemandJoinSkills(int employeeDemandId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@employeeDemandId", employeeDemandId);
            return await _dbClient.Query<object>("human.GetEmployeeDemandJoinSkills", p);
        }

        public async Task<object> GetEmployeeDemandJoinEducationDegrees(int employeeDemandId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@employeeDemandId", employeeDemandId);
            return await _dbClient.Query<object>("human.GetEmployeeDemandJoinEducationDegrees", p);
        }

        public async Task<object> GetEmployeeDemandJoinLanguages(int employeeDemandId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@employeeDemandId", employeeDemandId);
            return await _dbClient.Query<object>("human.GetEmployeeDemandJoinLanguages", p);
        }

        public async Task<object> GetEmployeeDemandJoinApprovalEmployees(int employeeDemandId, bool isModerator)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@employeeDemandId", employeeDemandId);
            p.Add("@isModerator", isModerator);
            return await _dbClient.Query<object>("human.GetEmployeeDemandJoinApprovalEmployees", p);
        }

        public async Task<object> GetEmployeeDemandJoinApprovalEmployeesHistory(int employeeDemandId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@employeeDemandId", employeeDemandId);
            return await _dbClient.Query<object>("human.GetEmployeeDemandJoinApprovalEmployeesHistory", p);
        }

        public async Task<GetMailTemplateResponse> GetMailTemplate(int code)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            return await _dbClient.QuerySingle<GetMailTemplateResponse>("admin.GetMailTemplate", p);
        }

        public async Task<List<GetMailEmployeeDemandJoinApprovalEmployeesResponse>> GetMailEmployeeDemandJoinApprovalEmployees(int employeeDemandId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@employeeDemandId", employeeDemandId);
            var result = await _dbClient.Query<GetMailEmployeeDemandJoinApprovalEmployeesResponse>("human.GetMailEmployeeDemandJoinApprovalEmployees", p);

            return result.ToList();
        }

        public async Task<object> SaveSentMailEmployeeDemandJoinApprovalEmployee(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("human.SaveSentMailEmployeeDemandJoinApprovalEmployee", p);
        }
        #endregion

        #region Employees
        public async Task<object> CreateAsEmployee(int jobOfferId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@jobOfferId", jobOfferId);
            return await _dbClient.QuerySingle<object>("human.CreateAsEmployee", p);
        }

        public async Task<object> GetEmployees(string code, string firstname, string middlename, string lastname, string position)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@firstname", firstname);
            p.Add("@middlename", middlename);
            p.Add("@lastname", lastname);
            p.Add("@position", position);
            return await _dbClient.Query<object>("human.GetEmployees", p);
        }

        public async Task<object> GetEmployee(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.GetEmployee", p);
        }

        public async Task<object> SaveEmployee(int id, int jobOfferId, int statusId, string startDate, string firstName, string middleName, string lastName, string IDNumber, string SIN, string TIN, string primaryEmail, int? citizenshipId, int? birthCountryId, int? birthCityId, string birthDate, int genderId, int? bloodGroupId, int? religionId, int employmentTypeId, int[] drivingCategories, int? militaryStatusId, int? maritalStatusId, int experience, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@jobOfferId", jobOfferId);
            p.Add("@statusId", statusId);
            p.Add("@startDate", startDate);
            p.Add("@firstName", firstName);
            p.Add("@middleName", middleName);
            p.Add("@lastName", lastName);
            p.Add("@IDNumber", IDNumber);
            p.Add("@SIN", SIN);
            p.Add("@TIN", TIN);
            p.Add("@primaryEmail", primaryEmail);
            p.Add("@citizenshipId", citizenshipId);
            p.Add("@birthCountryId", birthCountryId);
            p.Add("@birthCityId", birthCityId);
            p.Add("@birthDate", birthDate);
            p.Add("@genderId", genderId);
            p.Add("@bloodGroupId", bloodGroupId);
            p.Add("@religionId", religionId);
            p.Add("@employmentTypeId", employmentTypeId);
            p.Add("@drivingCategories", drivingCategories.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@militaryStatusId", militaryStatusId);
            p.Add("@maritalStatusId", maritalStatusId);
            p.Add("@experience", experience);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("human.SaveEmployee", p);
        }

        public async Task<object> DeleteEmployee(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteEmployee", p);
        }

        public async Task<object> GetEmployeesJoinUsers(int userId, string username, string code, string firstname, string middlename, string lastname, string position)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@username", username);
            p.Add("@code", code);
            p.Add("@firstname", firstname);
            p.Add("@middlename", middlename);
            p.Add("@lastname", lastname);
            p.Add("@position", position);
            return await _dbClient.Query<object>("human.GetEmployeesJoinUsers", p);
        }

        public async Task<object> GetEmployeeJoinDrivingCategories(int employeeId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@employeeId", employeeId);
            return await _dbClient.Query<object>("human.GetEmployeeJoinDrivingCategories", p);
        }
        #endregion

        #region Candidates
        public async Task<object> SaveCandidate(int id, int creatorUserId, string firstname, string middlename, string lastname, int? employmentTypeId, int? experience, int? sourceId, int? genderId, string note, string IDNumber, int? maritalStatusId, int? militaryStatusId, int? citizenshipId, int? birthCountryId, int? birthCityId, string birthDate, int[] drivingCategories, IEnumerable<LanguageArray> languages, IEnumerable<ContactArray> contacts, int[] skills, int[] educationDegrees)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@creatorUserId", creatorUserId);
            p.Add("@firstname", firstname);
            p.Add("@middlename", middlename);
            p.Add("@lastname", lastname);
            p.Add("@employmentTypeId", employmentTypeId);
            p.Add("@experience", experience);
            p.Add("@sourceId", sourceId);
            p.Add("@genderId", genderId);
            p.Add("@note", note);
            p.Add("@IDNumber", IDNumber);
            p.Add("@maritalStatusId", maritalStatusId);
            p.Add("@militaryStatusId", militaryStatusId);
            p.Add("@citizenshipId", citizenshipId);
            p.Add("@birthCountryId", birthCountryId);
            p.Add("@birthCityId", birthCityId);
            p.Add("@birthDate", birthDate);
            p.Add("@educationDegrees", educationDegrees.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@skills", skills.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@drivingCategories", drivingCategories.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@languages", languages.AsTableValuedParameters("eland.Language"));
            p.Add("@contacts", contacts.AsTableValuedParameters("eland.Contact"));
            return await _dbClient.QuerySingle<object>("human.SaveCandidate", p);
        }

        public async Task<object> DeleteCandidate(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteCandidate", p);
        }

        public async Task<object> GetCandidates(string code, string firstname, string middlename, string lastname, string source)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@firstname", firstname);
            p.Add("@middlename", middlename);
            p.Add("@lastname", lastname);
            p.Add("@source", source);
            return await _dbClient.Query<object>("human.GetCandidates", p);
        }

        public async Task<object> GetCandidate(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.GetCandidate", p);
        }

        public async Task<object> GetCandidateJoinSkills(int candidateId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@candidateId", candidateId);
            return await _dbClient.Query<object>("human.GetCandidateJoinSkills", p);
        }

        public async Task<object> GetCandidateJoinLanguages(int candidateId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@candidateId", candidateId);
            return await _dbClient.Query<object>("human.GetCandidateJoinLanguages", p);
        }

        public async Task<object> GetCandidateJoinContacts(int candidateId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@candidateId", candidateId);
            return await _dbClient.Query<object>("human.GetCandidateJoinContacts", p);
        }

        public async Task<object> GetCandidateJoinDrivingCategories(int candidateId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@candidateId", candidateId);
            return await _dbClient.Query<object>("human.GetCandidateJoinDrivingCategories", p);
        }

        public async Task<object> GetCandidateJoinEducationDegrees(int candidateId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@candidateId", candidateId);
            return await _dbClient.Query<object>("human.GetCandidateJoinEducationDegrees", p);
        }
        #endregion

        #region EmploymentTypes
        public async Task<object> GetEmploymentTypes()
        {
            return await _dbClient.Query<object>("human.GetEmploymentTypes");
        }
        #endregion

        #region Sources
        public async Task<object> GetSources()
        {
            return await _dbClient.Query<object>("human.GetSources");
        }
        #endregion

        #region MaritalStatuses
        public async Task<object> GetMaritalStatuses()
        {
            return await _dbClient.Query<object>("human.GetMaritalStatuses");
        }
        #endregion

        #region MilitaryStatuses
        public async Task<object> GetMilitaryStatuses()
        {
            return await _dbClient.Query<object>("human.GetMilitaryStatuses");
        }
        #endregion

        #region DrivingCategories
        public async Task<object> GetDrivingCategories()
        {
            return await _dbClient.Query<object>("human.GetDrivingCategories");
        }
        #endregion

        #region CitizenshipList
        public async Task<object> GetCitizenshipList()
        {
            return await _dbClient.Query<object>("human.GetCitizenshipList");
        }
        #endregion

        #region Applications
        public async Task<object> SaveApplication(int id, int creatorUserId, int employeeDemandId, int candidateId, int expectedSalary, int statusId, string note, int[] approvalEmployees, int[] moderators)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@creatorUserId", creatorUserId);
            p.Add("@employeeDemandId", employeeDemandId);
            p.Add("@candidateId", candidateId);
            p.Add("@expectedSalary", expectedSalary);
            p.Add("@statusId", statusId);
            p.Add("@note", note);
            p.Add("@approvalEmployees", approvalEmployees.ToList().AsTableValuedParameters("eland.Array"));
            p.Add("@moderators", moderators.ToList().AsTableValuedParameters("eland.Array"));
            return await _dbClient.QuerySingle<object>("human.SaveApplication", p);
        }

        public async Task<object> DeleteApplication(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteApplication", p);
        }

        public async Task<object> GetApplications(int? userId, string code, string employeeDemandCode, string department, string position)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@code", code);
            p.Add("@employeeDemandCode", employeeDemandCode);
            p.Add("@department", department);
            p.Add("@position", position);
            return await _dbClient.Query<object>("human.GetApplications", p);
        }

        public async Task<object> GetApplication(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.GetApplication", p);
        }

        public async Task<object> GetApplicationJoinApprovalEmployees(int applicationId, bool isModerator)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@applicationId", applicationId);
            p.Add("@isModerator", isModerator);
            return await _dbClient.Query<object>("human.GetApplicationJoinApprovalEmployees", p);
        }

        public async Task<object> GetApplicationJoinAppointments(int userId, int applicationId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@applicationId", applicationId);
            return await _dbClient.Query<object>("human.GetApplicationJoinAppointments", p);
        }

        public async Task<object> GetApplicationJoinInterviews(int applicationId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@applicationId", applicationId);
            var appHistory = await _dbClient.Query<AppointmentHistoryArray>("human.GetApplicationJoinInterviews", p);

            return from ah in appHistory
                   group new AppointmentDetails
                   {
                       Name = ah.Name,
                       Location = ah.Location,
                       Decision = ah.Decision,
                       FromDateTime = ah.FromDateTime,
                       InterviewType = ah.InterviewType,
                       Note = ah.Note,
                       ToDateTime = ah.ToDateTime
                   } by ah.FullName into myGroup
                   select new { FullName = myGroup.Key, Details = myGroup };
        }

        public async Task<object> IsReadOnlyApplication(int userId, int applicationId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@applicationId", applicationId);
            return await _dbClient.QuerySingle<object>("human.IsReadOnlyApplication", p);
        }
        #endregion

        #region InterviewTypes
        public async Task<object> GetInterviewTypes()
        {
            return await _dbClient.Query<object>("human.GetInterviewTypes");
        }
        #endregion

        #region JobOffers
        public async Task<object> GetJobOffers(string code, string applicationCode, string department, string position)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@applicationCode", applicationCode);
            p.Add("@department", department);
            p.Add("@position", position);
            return await _dbClient.Query<object>("human.GetJobOffers", p);
        }

        public async Task<object> GetJobOffer(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.GetJobOffer", p);
        }

        public async Task<object> DeleteJobOffer(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteJobOffer", p);
        }

        public async Task<object> SaveJobOffer(int id, int applicationId, int creatorUserId, int amount, int statusId, string offerDate, string offerExpirationDate, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@applicationId", applicationId);
            p.Add("@creatorUserId", creatorUserId);
            p.Add("@amount", amount);
            p.Add("@statusId", statusId);
            p.Add("@offerDate", offerDate);
            p.Add("@offerExpirationDate", offerExpirationDate);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("human.SaveJobOffer", p);
        }
        #endregion

        #region ProbationPeriods
        public async Task<object> GetProbationPeriods(int userId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            return await _dbClient.Query<object>("human.GetProbationPeriods", p);
        }

        public async Task<object> GetProbationPeriod(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.GetProbationPeriod", p);
        }

        public async Task<object> DeleteProbationPeriod(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteProbationPeriod", p);
        }

        public async Task<object> SaveProbationPeriod(int id, int statusId, int employeeId, int approvalEmployeeId, int creatorUserId, DateTime startDate, DateTime endDate, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@statusId", statusId);
            p.Add("@employeeId", employeeId);
            p.Add("@approvalEmployeeId", approvalEmployeeId);
            p.Add("@creatorUserId", creatorUserId);
            p.Add("@startDate", startDate);
            p.Add("@endDate", endDate);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("human.SaveProbationPeriod", p);
        }

        public async Task<object> GetProbationPeriodJoinSkills(int skillTypeCode, int? probationPeriodId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@skillTypeCode", skillTypeCode);
            p.Add("@probationPeriodId", probationPeriodId);
            return await _dbClient.Query<object>("human.GetProbationPeriodJoinSkills", p);
        }

        public async Task<object> SaveProbationPeriodJoinSkills(int skillId, int probationPeriodId, float evaluation)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@skillId", skillId);
            p.Add("@probationPeriodId", probationPeriodId);
            p.Add("@evaluation", evaluation);
            return await _dbClient.QuerySingle<object>("human.SaveProbationPeriodJoinSkills", p);
        }

        public async Task<object> IsReadOnlyProbationPeriod(int userId, int probationPeriodId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@userId", userId);
            p.Add("@probationPeriodId", probationPeriodId);
            return await _dbClient.QuerySingle<object>("human.IsReadOnlyProbationPeriod", p);
        }
        #endregion

        #region Appointments
        public async Task<object> SaveAppointment(int id, int userId, int applicationId, string name, string location, int decisionId, int interviewTypeId, string fromDateTime, string toDateTime, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@userId", userId);
            p.Add("@applicationId", applicationId);
            p.Add("@name", name);
            p.Add("@location", location);
            p.Add("@decisionId", decisionId);
            p.Add("@interviewTypeId", interviewTypeId);
            p.Add("@fromDateTime", fromDateTime);
            p.Add("@toDateTime", toDateTime);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("human.SaveAppointment", p);
        }

        public async Task<object> DeleteAppointment(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("human.DeleteAppointment", p);
        }
        #endregion

        #region BloodGroups
        public async Task<object> GetBloodGroups()
        {
            return await _dbClient.Query<object>("human.GetBloodGroups");
        }
        #endregion

        #region Religions
        public async Task<object> GetReligions()
        {
            return await _dbClient.Query<object>("human.GetReligions");
        }
        #endregion
    }
}
