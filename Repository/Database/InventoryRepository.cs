﻿using Dapper;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class InventoryRepository
    {
        readonly DBClient _dbClient;
        public InventoryRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region ItemTypes
        public async Task<object> GetItemTypes()
        {
            return await _dbClient.Query<object>("inventory.GetItemTypes");
        }

        public async Task<object> SaveItemType(int id, string name, string searchName, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@searchName", searchName);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("inventory.SaveItemType", p);
        }

        public async Task<object> DeleteItemType(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("inventory.DeleteItemType", p);
        }
        #endregion

        #region ItemCategories
        public async Task<object> GetItemCategories()
        {
            return await _dbClient.Query<object>("inventory.GetItemCategories");
        }

        public async Task<object> SaveItemCategory(int id, int parentId, string name, string searchName, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@parentId", parentId);
            p.Add("@name", name);
            p.Add("@searchName", searchName);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("inventory.SaveItemCategory", p);
        }

        public async Task<object> DeleteItemCategory(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("inventory.DeleteItemCategory", p);
        }
        #endregion

        #region UnitOfMeasures
        public async Task<object> GetUnitOfMeasures()
        {
            return await _dbClient.Query<object>("inventory.GetUnitOfMeasures");
        }

        public async Task<object> SaveUnitOfMeasure(int id, string name, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("inventory.SaveUnitOfMeasure", p);
        }

        public async Task<object> DeleteUnitOfMeasure(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("inventory.DeleteUnitOfMeasure", p);
        }
        #endregion

        #region Items
        public async Task<object> GetItems()
        {
            return await _dbClient.Query<object>("inventory.GetItems");
        }

        public async Task<object> SaveItem(int id, string description, int baseUnitOfMeasureId, int itemCategoryId, int itemTypeId, decimal unitPrice, decimal unitCost)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@description", description);
            p.Add("@baseUnitOfMeasureId", baseUnitOfMeasureId);
            p.Add("@itemCategoryId", itemCategoryId);
            p.Add("@itemTypeId", itemTypeId);
            p.Add("@unitPrice", unitPrice);
            p.Add("@unitCost", unitCost);
            return await _dbClient.QuerySingle<object>("inventory.SaveItem", p);
        }

        public async Task<object> DeleteItem(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("inventory.DeleteItem", p);
        }

        public async Task<object> GetItem(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("inventory.GetItem", p);
        }
        #endregion
    }
}
