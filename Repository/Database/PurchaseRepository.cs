﻿using Dapper;
using Mainspace.Models.Request;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class PurchaseRepository
    {
        readonly DBClient _dbClient;
        public PurchaseRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region Vendors
        public async Task<object> GetVendors()
        {
            return await _dbClient.Query<object>("purchase.GetVendors");
        }

        public async Task<object> SaveVendor(int id, string name, string searchName, string note, VendorJoinContacts[] contacts, VendorJoinAddresses[] addresses)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@searchName", searchName);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("purchase.SaveVendor", p);
        }

        public async Task<object> DeleteVendor(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.DeleteVendor", p);
        }

        public async Task<object> GetVendor(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.GetVendor", p);
        }
        #endregion

        #region VendorJoinContacts
        public async Task<object> SaveVendorJoinContact(int id, int vendorId, int contactTypeId, string contact)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@vendorId", vendorId);
            p.Add("@contactTypeId", contactTypeId);
            p.Add("@contact", contact);
            return await _dbClient.QuerySingle<object>("purchase.SaveVendorJoinContact", p);
        }
        #endregion

        #region VendorJoinAddresses
        public async Task<object> SaveVendorJoinAddress(int id, int vendorId, int addressTypeId, string address)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@vendorId", vendorId);
            p.Add("@addressTypeId", addressTypeId);
            p.Add("@address", address);
            return await _dbClient.QuerySingle<object>("purchase.SaveVendorJoinAddress", p);
        }
        #endregion

        #region PurchaseOrders
        public async Task<object> GetPurchaseOrders()
        {
            return await _dbClient.Query<object>("purchase.GetPurchaseOrders");
        }

        public async Task<object> SavePurchaseOrder(int id, int vendorId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@vendorId", vendorId);
            return await _dbClient.QuerySingle<object>("purchase.SavePurchaseOrder", p);
        }

        public async Task<object> DeletePurchaseOrder(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.DeletePurchaseOrder", p);
        }

        public async Task<object> GetPurchaseOrder(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.GetPurchaseOrder", p);
        }
        #endregion

        #region BlanketPurchaseOrders
        public async Task<object> GetBlanketPurchaseOrders()
        {
            return await _dbClient.Query<object>("purchase.GetBlanketPurchaseOrders");
        }

        public async Task<object> SaveBlanketPurchaseOrder(int id, int vendorId, System.DateTime documentDate, System.DateTime orderDate, System.DateTime dueDate, string vendorShipmentNumber, string vendorOrderNumber)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@vendorId", vendorId);
            p.Add("@documentDate", documentDate);
            p.Add("@orderDate", orderDate);
            p.Add("@dueDate", dueDate);
            p.Add("@vendorShipmentNumber", vendorShipmentNumber);
            p.Add("@vendorOrderNumber", vendorOrderNumber);
            return await _dbClient.QuerySingle<object>("purchase.SaveBlanketPurchaseOrder", p);
        }

        public async Task<object> DeleteBlanketPurchaseOrder(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.DeleteBlanketPurchaseOrder", p);
        }

        public async Task<object> GetBlanketPurchaseOrder(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.GetBlanketPurchaseOrder", p);
        }
        #endregion

        #region BlanketPurchaseOrderLines
        public async Task<object> GetBlanketPurchaseOrderLines(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("purchase.GetBlanketPurchaseOrderLines", p);
        }

        public async Task<object> SaveBlanketPurchaseOrderLine(int id, int purchaseOrderId, int itemId, int unitOfMeasureId, decimal quantity, int locationId, decimal quantityReceived, decimal quantityInvoiced, System.DateTime expectedReceiptDate)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@blanketPurchaseOrderId", purchaseOrderId);
            p.Add("@itemId", itemId);
            p.Add("@unitOfMeasureId", unitOfMeasureId);
            p.Add("@quantity", quantity);
            p.Add("@locationId", locationId);
            p.Add("@quantityReceived", quantityReceived);
            p.Add("@quantityInvoiced", quantityInvoiced);
            p.Add("@expectedReceiptDate", expectedReceiptDate);
            return await _dbClient.QuerySingle<object>("purchase.SaveBlanketPurchaseOrderLine", p);
        }

        public async Task<object> DeleteBlanketPurchaseOrderLine(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.DeleteBlanketPurchaseOrderLine", p);
        }
        #endregion

        #region PurchaseOrderLines
        public async Task<object> GetPurchaseOrderLines()
        {
            return await _dbClient.Query<object>("purchase.GetPurchaseOrderLines");
        }

        public async Task<object> SavePurchaseOrderLine(int id, int purchaseOrderId, int itemId, int quantity, int locationId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@purchaseOrderId", purchaseOrderId);
            p.Add("@itemId", itemId);
            p.Add("@quantity", quantity);
            p.Add("@locationId", locationId);
            return await _dbClient.QuerySingle<object>("purchase.SavePurchaseOrderLine", p);
        }

        public async Task<object> DeletePurchaseOrderLine(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("purchase.DeletePurchaseOrderLine", p);
        }
        #endregion
    }
}
