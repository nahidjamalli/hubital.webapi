﻿using Dapper;
using System;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class ReportRepository
    {
        readonly DBClient _dbClient;
        public ReportRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region TrialBalance
        public async Task<object> GetTrialBalance(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@startDate", startDate);
            p.Add("@endDate", endDate);
            return await _dbClient.Query<object>("report.GetTrialBalance", p);
        }
        #endregion

        #region AccountListing
        public async Task<object> GetAccountListing(int accountId, DateTimeOffset startDate, DateTimeOffset endDate)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@accountId", accountId);
            p.Add("@startDate", startDate);
            p.Add("@endDate", endDate);
            return await _dbClient.Query<object>("report.GetAccountListing", p);
        }
        #endregion

        #region GeneralLedgerHistory
        public async Task<object> GetGeneralLedgerHistory(DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@startDate", startDate);
            p.Add("@endDate", endDate);
            return await _dbClient.Query<object>("report.GetGeneralLedgerHistory", p);
        }
        #endregion


        #region AccountsAging
        public async Task<object> GetAccountsAging(DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@startDate", startDate);
            p.Add("@endDate", endDate);
            return await _dbClient.Query<object>("report.GetAccountsAging", p);
        }
        #endregion
    }
}
