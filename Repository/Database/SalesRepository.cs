﻿using Dapper;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class SalesRepository
    {
        readonly DBClient _dbClient;
        public SalesRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region Customers
        public async Task<object> GetCustomers()
        {
            return await _dbClient.Query<object>("sales.GetCustomers");
        }

        public async Task<object> SaveCustomer(int id, string name, string searchName, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@searchName", searchName);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("sales.SaveCustomer", p);
        }

        public async Task<object> DeleteCustomer(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("sales.DeleteCustomer", p);
        }

        public async Task<object> GetCustomer(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("sales.GetCustomer", p);
        }
        #endregion
    }
}
