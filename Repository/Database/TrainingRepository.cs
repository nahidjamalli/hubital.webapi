﻿using Dapper;
using Mainspace.Repository.Extension;
using System.Linq;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class TrainingRepository
    {
        readonly DBClient _dbClient;
        public TrainingRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region ExamTypes
        public async Task<object> GetExamTypes()
        {
            return await _dbClient.Query<object>("training.GetExamTypes");
        }
        #endregion

        #region ExamCandidates
        public async Task<object> GetExamCandidates()
        {
            return await _dbClient.Query<object>("training.GetExamCandidates");
        }

        public async Task<object> GetExamCandidateJoinAnswers(int examHistoryId, int questionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@examHistoryId", examHistoryId);
            p.Add("@questionId", questionId);
            return await _dbClient.Query<object>("training.GetExamCandidateJoinAnswers", p);
        }

        public async Task<object> GetExamCandidate(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.GetExamCandidate", p);
        }

        public async Task<object> SaveExamCandidate(int id, int examId, int candidateId, bool showResult, bool isEmployee, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@examId", examId);
            p.Add("@candidateId", candidateId);
            p.Add("@showResult", showResult);
            p.Add("@isEmployee", isEmployee);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("training.SaveExamCandidate", p);
        }

        public async Task<object> DeleteExamCandidate(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.DeleteExamCandidate", p);
        }
        #endregion

        #region Exams
        public async Task<object> SaveExam(int id, string name, int examTypeId, string languageId, int requiredScore, int duration, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@examTypeId", examTypeId);
            p.Add("@languageId", languageId);
            p.Add("@requiredScore", requiredScore);
            p.Add("@duration", duration);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("training.SaveExam", p);
        }

        public async Task<object> GetExams(string code, string exampType, string language, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            p.Add("@name", name);
            p.Add("@examType", exampType);
            p.Add("@language", language);
            return await _dbClient.Query<object>("training.GetExams", p);
        }

        public async Task<object> GetExam(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.GetExam", p);
        }

        public async Task<object> GetCurrentExam(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.GetCurrentExam", p);
        }

        public async Task<object> DeleteExam(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.DeleteExam", p);
        }

        public async Task<object> DeleteExamJoinRule(int ruleId, int examId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@ruleId", ruleId);
            p.Add("@examId", examId);
            return await _dbClient.QuerySingle<object>("training.DeleteExamJoinRule", p);
        }

        public async Task<object> UpdateExamJoinQuestion(int id, int order)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@order", order);
            return await _dbClient.QuerySingle<object>("training.UpdateExamJoinQuestion", p);
        }

        public async Task<object> DeleteExamJoinQuestion(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.DeleteExamJoinQuestion", p);
        }

        public async Task<object> SaveExamJoinRule(int ruleId, int examId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@ruleId", ruleId);
            p.Add("@examId", examId);
            return await _dbClient.QuerySingle<object>("training.SaveExamJoinRule", p);
        }

        public async Task<object> SaveExamJoinQuestion(int questionId, int examId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@questionId", questionId);
            p.Add("@examId", examId);
            return await _dbClient.QuerySingle<object>("training.SaveExamJoinQuestion", p);
        }

        public async Task<object> GetExamJoinRules(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("training.GetExamJoinRules", p);
        }

        public async Task<object> GetExamJoinQuestions(int examId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@examId", examId);
            return await _dbClient.Query<object>("training.GetExamJoinQuestions", p);
        }

        public async Task<object> ExamCandidateSignIn(string code)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            return await _dbClient.QuerySingle<object>("training.SignIn", p);
        }

        public async Task<object> StartExam(string code)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@code", code);
            return await _dbClient.QuerySingle<object>("training.StartExam", p);
        }

        public async Task<object> StopExam(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.StopExam", p);
        }

        public async Task<object> GetExamResult(int examHistoryId, int examId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@examId", examId);
            p.Add("@examHistoryId", examHistoryId);
            return await _dbClient.QuerySingle<object>("training.GetExamResult", p);
        }
        #endregion

        #region Questions
        public async Task<object> SaveQuestion(int id, string name, int questionGroupId, int questionTypeId, int score, int duration, string note)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@questionGroupId", questionGroupId);
            p.Add("@questionTypeId", questionTypeId);
            p.Add("@score", score);
            p.Add("@duration", duration);
            p.Add("@note", note);
            return await _dbClient.QuerySingle<object>("training.SaveQuestion", p);
        }

        public async Task<object> GetQuestions(string questionGroup, string questionType, string code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@questionGroup", questionGroup);
            p.Add("@questionType", questionType);
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.Query<object>("training.GetQuestions", p);
        }

        public async Task<object> GetQuestion(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.GetQuestion", p);
        }

        public async Task<object> DeleteQuestion(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.DeleteQuestion", p);
        }

        public async Task<object> GetCurrentQuestion(int examId, int order)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@examId", examId);
            p.Add("@order", order);
            return await _dbClient.QuerySingle<object>("training.GetCurrentQuestion", p);
        }

        public async Task<object> GetQuestionTypes()
        {
            return await _dbClient.Query<object>("training.GetQuestionTypes");
        }

        public async Task<object> GetQuestionGroups()
        {
            return await _dbClient.Query<object>("training.GetQuestionGroups");
        }

        public async Task<object> GetQuestionJoinContents(int questionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@questionId", questionId);
            return await _dbClient.Query<object>("training.GetQuestionJoinContents", p);
        }

        public async Task<object> DeleteContent(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.DeleteContent", p);
        }

        public async Task<object> SaveContent(int id, int questionId, int answerId, string content, short type, byte order)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@questionId", questionId);
            p.Add("@answerId", answerId);
            p.Add("@content", content);
            p.Add("@type", type);
            p.Add("@order", order);
            return await _dbClient.QuerySingle<object>("training.SaveContent", p);
        }
        #endregion

        #region Answers
        public async Task<object> GetCurrentQuestionJoinAnswers(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.Query<object>("training.GetCurrentQuestionJoinAnswers", p);
        }

        public async Task<object> SaveQuestionJoinAnswers(int examHistoryId, int timeleft, int questionId, int[] answers)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@examHistoryId", examHistoryId);
            p.Add("@timeleft", timeleft);
            p.Add("@questionId", questionId);
            p.Add("@answers", answers.ToList().AsTableValuedParameters("elan.Array"));
            return await _dbClient.QuerySingle<object>("training.SaveQuestionJoinAnswers", p);
        }

        public async Task<object> SaveQuestionJoinOpenAnswer(int examHistoryId, int timeleft, int questionId, string answer)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@examHistoryId", examHistoryId);
            p.Add("@timeleft", timeleft);
            p.Add("@questionId", questionId);
            p.Add("@answer", answer);
            return await _dbClient.QuerySingle<object>("training.SaveQuestionJoinOpenAnswer", p);
        }

        public async Task<object> GetQuestionJoinAnswers(int questionId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@questionId", questionId);
            return await _dbClient.Query<object>("training.GetQuestionJoinAnswers", p);
        }

        public async Task<object> GetAnswerJoinContents(int answerId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@answerId", answerId);
            return await _dbClient.Query<object>("training.GetAnswerJoinContents", p);
        }

        public async Task<object> DeleteAnswer(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.DeleteAnswer", p);
        }

        public async Task<object> SaveAnswer(int id, int questionId, string name, byte order, bool correct)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@questionId", questionId);
            p.Add("@name", name);
            p.Add("@order", order);
            p.Add("@correct", correct);
            return await _dbClient.QuerySingle<object>("training.SaveAnswer", p);
        }
        #endregion

        #region Rules
        public async Task<object> GetRules()
        {
            return await _dbClient.Query<object>("training.GetRules");
        }

        public async Task<object> DeleteRule(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("training.DeleteRule", p);
        }

        public async Task<object> SaveRule(int id, int code, string name)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@name", name);
            return await _dbClient.QuerySingle<object>("training.SaveRule", p);
        }
        #endregion
    }
}
