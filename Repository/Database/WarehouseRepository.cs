﻿using Dapper;
using System.Threading.Tasks;

namespace Mainspace.Repository.Database
{
    public class WarehouseRepository
    {
        readonly DBClient _dbClient;
        public WarehouseRepository(DBClient dbClient)
        {
            _dbClient = dbClient;
        }

        #region BinTypes
        public async Task<object> GetBinTypes()
        {
            return await _dbClient.Query<object>("warehouse.GetBinTypes");
        }

        public async Task<object> SaveBinType(int id, string code, string description, bool receive, bool ship, bool putAway, bool pick)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            p.Add("@receive", receive);
            p.Add("@ship", ship);
            p.Add("@putAway", putAway);
            p.Add("@pick", pick);
            return await _dbClient.QuerySingle<object>("warehouse.SaveBinType", p);
        }

        public async Task<object> DeleteBinType(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("warehouse.DeleteBinType", p);
        }
        #endregion

        #region Zones
        public async Task<object> GetZones()
        {
            return await _dbClient.Query<object>("warehouse.GetZones");
        }

        public async Task<object> SaveZone(int id, string code, string description, int binTypeId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            p.Add("@binTypeId", binTypeId);
            return await _dbClient.QuerySingle<object>("warehouse.SaveZone", p);
        }

        public async Task<object> DeleteZone(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("warehouse.DeleteZone", p);
        }
        #endregion

        #region Bins
        public async Task<object> GetBins()
        {
            return await _dbClient.Query<object>("warehouse.GetBins");
        }

        public async Task<object> SaveBin(int id, string code, string description, int zoneId, int binTypeId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@code", code);
            p.Add("@description", description);
            p.Add("@zoneId", zoneId);
            p.Add("@binTypeId", binTypeId);
            return await _dbClient.QuerySingle<object>("warehouse.SaveBin", p);
        }

        public async Task<object> DeleteBin(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("warehouse.DeleteBin", p);
        }
        #endregion

        #region Locations
        public async Task<object> GetLocations()
        {
            return await _dbClient.Query<object>("warehouse.GetLocations");
        }

        public async Task<object> SaveLocation(int id, string name, string searchName)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            p.Add("@name", name);
            p.Add("@searchName", searchName);
            return await _dbClient.QuerySingle<object>("warehouse.SaveLocation", p);
        }

        public async Task<object> DeleteLocation(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@id", id);
            return await _dbClient.QuerySingle<object>("warehouse.DeleteLocation", p);
        }
        #endregion
    }
}
