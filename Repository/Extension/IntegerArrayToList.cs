﻿using Mainspace.Models.Response;
using System.Collections.Generic;

namespace Mainspace.Repository.Extension
{
    public static class IntegerArrayToList
    {
        public static List<IntegerValueArray> ToList(this int[] arr)
        {
            List<IntegerValueArray> list = new List<IntegerValueArray>();

            if (arr != null)
                for (int j = 0; j < arr.Length; j++)
                {
                    list.Add(new IntegerValueArray
                    {
                        Value = arr[j]
                    });
                }

            return list;
        }
    }
}
