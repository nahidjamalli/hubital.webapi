﻿using Dapper;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace Mainspace.Repository.Extension
{
    public class Tvp : SqlMapper.ICustomQueryParameter
    {
        public static readonly Action<SqlParameter, string> SetTypeName;
        private readonly IEnumerable<SqlDataRecord> _table;
        private readonly string _typeName;

        /// <summary>
        ///     Create a new instance of TableValuedParameter
        /// </summary>
        public Tvp(IEnumerable<SqlDataRecord> table) : this(table, null)
        {
        }

        /// <summary>
        ///     Create a new instance of TableValuedParameter
        /// </summary>
        public Tvp(IEnumerable<SqlDataRecord> table, string typeName)
        {
            _table = table;
            _typeName = typeName;
        }


        void SqlMapper.ICustomQueryParameter.AddParameter(IDbCommand command, string name)
        {
            var param = command.CreateParameter();
            param.ParameterName = name;
            Set(param, _table, _typeName);
            command.Parameters.Add(param);
        }

        private static void Set(IDbDataParameter parameter, IEnumerable<SqlDataRecord> table, string typeName)
        {
#pragma warning disable CS0618 // Type or member is obsolete
            parameter.Value = SqlMapper.SanitizeParameterValue(table);
#pragma warning restore CS0618 // Type or member is obsolete

            if (parameter is SqlParameter sqlParam)
            {
                SetTypeName?.Invoke(sqlParam, typeName);
                sqlParam.SqlDbType = SqlDbType.Structured;
            }
        }
    }
    public static class TableValuedParameter
    {
        private static readonly Dictionary<Type, SqlDbType> Types = new Dictionary<Type, SqlDbType>
        {
            {typeof(bool), SqlDbType.Bit},
            {typeof(bool?), SqlDbType.Bit},
            {typeof(byte), SqlDbType.TinyInt},
            {typeof(byte?), SqlDbType.TinyInt},
            {typeof(string), SqlDbType.NVarChar},
            {typeof(DateTime), SqlDbType.DateTime2},
            {typeof(DateTime?), SqlDbType.DateTime2},
            {typeof(short), SqlDbType.SmallInt},
            {typeof(short?), SqlDbType.SmallInt},
            {typeof(int), SqlDbType.Int},
            {typeof(int?), SqlDbType.Int},
            {typeof(long), SqlDbType.BigInt},
            {typeof(long?), SqlDbType.BigInt},
            {typeof(decimal), SqlDbType.Decimal},
            {typeof(decimal?), SqlDbType.Decimal},
            {typeof(double), SqlDbType.Float},
            {typeof(double?), SqlDbType.Float},
            {typeof(float), SqlDbType.Real},
            {typeof(float?), SqlDbType.Real},
            {typeof(TimeSpan), SqlDbType.Time},
            {typeof(Guid), SqlDbType.UniqueIdentifier},
            {typeof(Guid?), SqlDbType.UniqueIdentifier},
            {typeof(byte[]), SqlDbType.Binary},
            {typeof(byte?[]), SqlDbType.Binary},
            {typeof(char[]), SqlDbType.Char},
            {typeof(char?[]), SqlDbType.Char}
        };

        private static readonly ConcurrentDictionary<Type, SqlMetaData[]> Tipler =
            new ConcurrentDictionary<Type, SqlMetaData[]>();

        private static readonly ConcurrentDictionary<Type, PropertyInfo[]> Props =
            new ConcurrentDictionary<Type, PropertyInfo[]>();

        public static SqlDbType GetSqlDbType(this Type systype)
        {
            var resulttype = SqlDbType.NVarChar;
            Types.TryGetValue(systype, out resulttype);
            return resulttype;
        }

        public static IEnumerable<SqlDataRecord> ConvertToTvp<T>(this IEnumerable<T> data) where T : new()
        {
            SqlMetaData[] gelenData;
            PropertyInfo[] properties;

            if (Tipler.ContainsKey(typeof(T)))
            {
                gelenData = Tipler[typeof(T)];
                if (Props.ContainsKey(typeof(T)))
                {
                    properties = Props[typeof(T)];
                }
                else
                {
                    properties = typeof(T).GetTypeInfo().GetProperties();
                    Props.TryAdd(typeof(T), properties);
                }
            }
            else
            {
                var records = new List<SqlMetaData>();
                if (Props.ContainsKey(typeof(T)))
                {
                    properties = Props[typeof(T)];
                }
                else
                {
                    properties = typeof(T).GetTypeInfo().GetProperties();
                    Props.TryAdd(typeof(T), properties);
                }

                foreach (var prop in properties)
                {
                    var pt = prop.PropertyType;
                    var sdbtyp = prop.PropertyType.GetSqlDbType();
                    records.Add(pt.Name.Equals("String")
                        ? new SqlMetaData(prop.Name, sdbtyp, 4000)
                        : new SqlMetaData(prop.Name, sdbtyp));
                }

                gelenData = records.ToArray();
                Tipler.TryAdd(typeof(T), gelenData);
            }

            var ret = new SqlDataRecord(gelenData);
            foreach (var d in data)
            {
                for (var i = 0; i < properties.Length; i++)
                {
                    var im = properties[i];
                    ret.SetValue(i, properties[i].GetValue(d, null));
                }

                yield return ret;
            }
        }

        /// <summary>
        ///     This extension converts an enumerable set to a Dapper TVP
        /// </summary>
        /// <typeparam name="T">type of enumerbale</typeparam>
        /// <param name="enumerable">list of values</param>
        /// <param name="orderedColumnNames">if more than one column in a TVP, columns order must mtach order of columns in TVP</param>
        /// <returns>a custom query parameter</returns>
        public static SqlMapper.ICustomQueryParameter AsTableValuedParameters<T>(this IEnumerable<T> enumerable,
            string typeName = null) where T : new()
        {
            string attrName;
            if (string.IsNullOrEmpty(typeName))
                attrName = GetDbTypeName<T>();
            else
                attrName = typeName;

            var table = enumerable.ConvertToTvp();

            if (enumerable.Count() > 0) return new Tvp(table, attrName);
            else
            {
                var e = enumerable.GetEnumerator();
                e.MoveNext();
                return new Tvp(GetEmpty(e.Current), attrName);
            }

            //new List<SqlDataRecord> { new SqlDataRecord(new SqlMetaData[] { new SqlMetaData("Value", SqlDbType.Int) }) }
        }

        public static string GetDbTypeName<T>()
        {
            var dnAttribute = typeof(T).GetTypeInfo().GetCustomAttributes(
                typeof(DbTypeNameAttribute), true
            ).FirstOrDefault() as DbTypeNameAttribute;
            return dnAttribute?.Name;
        }

        public static List<SqlDataRecord> GetEmpty<T>(T data) where T : new()
        {
            var records = new List<SqlMetaData>();
            SqlMetaData[] gelenData;
            PropertyInfo[] properties;

            if (Props.ContainsKey(typeof(T)))
            {
                properties = Props[typeof(T)];
            }
            else
            {
                properties = typeof(T).GetTypeInfo().GetProperties();
                Props.TryAdd(typeof(T), properties);
            }

            foreach (var prop in properties)
            {
                var pt = prop.PropertyType;
                var sdbtyp = prop.PropertyType.GetSqlDbType();
                records.Add(pt.Name.Equals("String")
                    ? new SqlMetaData(prop.Name, sdbtyp, 4000)
                    : new SqlMetaData(prop.Name, sdbtyp));
            }

            gelenData = records.ToArray();
            return new List<SqlDataRecord>() { new SqlDataRecord(gelenData) };
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class DbTypeNameAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
