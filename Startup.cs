using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Mainspace
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var sqlConf = Configuration.GetSection("SQLConfiguration").Get<Models.Configuration.SQLConnection>();

            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddSingleton(sqlConf);
            services.AddSingleton<Repository.Database.ContractManagementRepository>();
            services.AddSingleton<Repository.Database.ConfigurationRepository>();
            services.AddSingleton<Repository.Database.AdministrationRepository>();
            services.AddSingleton<Repository.Database.HumanResourcesRepository>();
            services.AddSingleton<Repository.Database.TrainingRepository>();
            services.AddSingleton<Repository.Database.FinanceRepository>();
            services.AddSingleton<Repository.Database.InventoryRepository>();
            services.AddSingleton<Repository.Database.WarehouseRepository>();
            services.AddSingleton<Repository.Database.SalesRepository>();
            services.AddSingleton<Repository.Database.PurchaseRepository>();
            services.AddSingleton<Repository.Database.ReportRepository>();
            services.AddSingleton<Repository.Database.DBClient>();
            services.AddControllers();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");
            //app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
